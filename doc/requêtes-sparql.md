Requêtes SPARQL 
===============
<!-- TOC -->
1. [Prefix](#prefix)
2. [requête générique pour récupérer au format lisible le maximum d'infos sur toutes les entrées lexicales](#requête-générique-pour-récupérer-au-format-lisible-le-maximum-dinfos-sur-toutes-les-entrées-lexicales)
3. [Compter le nombre d'entrées lexicales et de définitions](#compter-le-nombre-dentrées-lexicales-et-de-définitions)
4. [Construct pour exporter des données de test](#construct-pour-exporter-des-données-de-test)

<!-- /TOC -->

# Prefix 
Insérer les préfixes suivant en début de requête. 

**NB** : Inutile pour GraphDB car il les ajoute automatiquement

# requête générique pour récupérer au format lisible le maximum d'infos sur toutes les entrées lexicales

```sql
SELECT distinct ?mot ?etym ?formLoc ?formItem ?pos ?gen ?num ?trans ?def ?reg ?con ?soc ?dom ?temp ?glossary ?ref ?ex ?senseItem ?senseLoc ?relations ?relatedForms 
#FROM <http://data.dictionnairedesfrancophones.org/dict/test/graph>
#FROM <http://data.dictionnairedesfrancophones.org/dict/voc/graph>
#FROM <http://data.dictionnairedesfrancophones.org/dict/ontologies/graph>
WHERE {
   	#VALUES ?mot { "Bello"@fr "chaîne"@fr "affaire"@fr "jaquette"@fr}
    # lien lexicalEntry - forme
    ?lentry ontolex:canonicalForm ?form .
    ?form ontolex:writtenRep ?mot .
    optional { ?form ddf:hasItemAboutForm/sioc:content ?formItem } .
    optional { 
    	SELECT ?form (GROUP_CONCAT(?fphon; SEPARATOR=", ") AS ?phon)
        WHERE{ ?form ontolex:phoneticRep ?fphon }
 	    GROUP BY ?form } .
    # regroupement des localisations d'une forme
    optional {
        SELECT ?form (GROUP_CONCAT(?floc; SEPARATOR=", ") AS ?formLoc)
        WHERE{
            ?form ddf:formHasLocalisation/skos:prefLabel ?floc }
 	    GROUP BY ?form}
	# Détails sur la LexicalEntry
    optional { ?lentry ddf:hasItemAboutEtymology/sioc:content ?etym } .
    ## infos pouvant être dupliquées pour une même lexicalEntry
	optional { SELECT ?lentry (GROUP_CONCAT(?g; SEPARATOR=", ") AS ?gen)
        WHERE{ ?lentry ddf:hasGender/skos:prefLabel ?g .		}
 	    GROUP BY ?lentry}.
    optional { SELECT ?lentry (GROUP_CONCAT(?n; SEPARATOR=", ") AS ?num)
        WHERE{ ?lentry ddf:hasNumber/skos:prefLabel ?n }
        GROUP BY ?lentry}.
    optional { SELECT ?lentry (GROUP_CONCAT(?tran; SEPARATOR=", ") AS ?trans)
        WHERE{ ?lentry ddf:hasTransitivity/skos:prefLabel ?tran }
        GROUP BY ?lentry}.
    optional { SELECT ?lentry (GROUP_CONCAT(?p; SEPARATOR=", ") AS ?pos)
        WHERE{ ?lentry ddf:hasPartOfSpeech/skos:prefLabel ?p }
        GROUP BY ?lentry}.
    # Détails sur le LexicalSense
    ?lentry ontolex:sense ?sense .
    optional{ ?sense skos:definition ?def . }
    optional{ ?sense ddf:hasRegister/skos:prefLabel ?reg } .
    optional{ ?sense ddf:hasTemporality/skos:prefLabel ?temp } .
     optional{ ?sense ddf:hasConnotation/skos:prefLabel ?con } .
    optional{ ?sense ddf:hasSociolect/skos:prefLabel ?soc } .
    optional{ ?sense ddf:hasDomain/skos:prefLabel ?dom } .
    optional{ ?sense ddf:hasItemAboutSense/sioc:content ?senseItem} .
    optional{ ?sense lexicog:usageExample ?usex .
    	optional{ ?usex dct:bibliographicalCitation ?ref . }
    	optional{ ?usex rdf:value ?ex .  } } .
    optional {
        SELECT ?sense (GROUP_CONCAT(?loc; SEPARATOR=", ") AS ?senseLoc)
        WHERE{
            ?sense ddf:hasLocalisation/skos:prefLabel ?loc }
 	    GROUP BY ?sense}
    optional {
        SELECT ?sense (GROUP_CONCAT(?gloss; SEPARATOR=", ") AS ?glossary)
        WHERE{
            ?sense ddf:hasGlossary/skos:prefLabel ?gloss }
 	    GROUP BY ?sense}
    # relations sense-entry
    optional { 
        SELECT ?sense (GROUP_CONCAT(?relatedForm; SEPARATOR=", ") AS ?relatedForms) (GROUP_CONCAT(?srel; SEPARATOR=", ") AS ?relations)
        WHERE{
            ?sense ddf:lexicalSenseHasSemanticRelationWith ?sr .
       		?sr ddf:hasSemanticRelationType/skos:prefLabel ?srel .
        	optional{
                ?sr ddf:semanticRelationOfEntry/lexicog:describes/ontolex:canonicalForm/ontolex:writtenRep ?relatedForm 
            }
        }
 		GROUP BY ?sense
    	}
	}
ORDER BY ?mot
limit 1000
```

# Compter le nombre d'entrées lexicales et de définitions
```sql
SELECT ?nbEntréesLexcicales ?nbDéfinitions ?graph
#  
WHERE {
    GRAPH ?graph {
    	{SELECT DISTINCT (COUNT(?lentry) as ?nbEntréesLexcicales) ?graph 
       	 WHERE {
            VALUES ?graph {<http://data.dictionnairedesfrancophones.org/dict/wik/graph/17Sep2019> dinv:graph}
         	?entry lexicog:describes ?lentry . 
            }
         GROUP BY ?graph
        }
        UNION
        {SELECT DISTINCT  (COUNT(?sense) as ?nbDéfinitions) ?graph 
       	 WHERE {
         	VALUES ?graph {<http://data.dictionnairedesfrancophones.org/dict/wik/graph/17Sep2019> dinv:graph}
			?lentry ontolex:sense ?sense .
            }
         GROUP BY ?graph
        }
	}
}
ORDER BY ?graph
```

# Construct pour exporter des données de test

```sql
CONSTRUCT {
    ?entry lexicog:describes ?lentry . 
    ?entry ddf:hasLexicographicResource ?res .
	?lentry ontolex:canonicalForm ?form .
    ?form ontolex:writtenRep ?mot .
	?form ddf:hasItemAboutForm  ?formItemN .
    ?formItemN sioc:content ?formItem .
    ?form ddf:formHasLocalisation ?loc .
	?lentry ddf:hasItemAboutEtymology ?etymN .
    ?etymN sioc:content ?etym .
 	?lentry ddf:hasGender ?g .		
    ?lentry ddf:hasNumber ?n .
	?lentry ddf:hasTransitivity ?tran .
	?lentry ddf:hasPartOfSpeech ?p .
    ?lentry ontolex:sense ?sense .
    ?sense skos:definition ?def . 
    ?sense ddf:hasRegister ?reg  .
    ?sense ddf:hasTemporality ?temp  .
    ?sense ddf:hasConnotation ?con  .
    ?sense ddf:hasSociolect ?soc .
    ?sense ddf:hasDomain ?dom  .
    ?sense ddf:hasItemAboutSense ?senseItemN .
    ?senseItemN sioc:content ?senseItem .
	?sense lexicog:usageExample ?usex .
 	?usex dct:bibliographicalCitation ?ref . 
    ?usex rdf:value ?ex .  
	?sense ddf:hasLocalisation ?loc .
    ?sense ddf:hasGlossary ?gloss .
    ?sense ddf:lexicalSenseHasSemanticRelationWith ?sr .
	?sr ddf:hasSemanticRelationType ?srel .
	?sr ddf:semanticRelationOfEntry ?relentry .
    ?relentry lexicog:describes ?relLentry .
    ?relLentry ontolex:canonicalForm ?relForm .
    ?relForm ontolex:writtenRep ?relatedForm  .  
}
WHERE {
   	#VALUES ?mot { "jaquette"@fr}
    # lien lexicalEntry - forme
    ?entry lexicog:describes ?lentry . 
    ?entry ddf:hasLexicographicResource ?res .
    ?lentry ontolex:canonicalForm ?form .
    ?form ontolex:writtenRep ?mot .
    optional { 
        ?form ddf:hasItemAboutForm ?formItemN .
      	?formItemN sioc:content ?formItem } .
    # localisations d'une forme
    optional { ?form ddf:formHasLocalisation ?floc }  
	# Détails sur la LexicalEntry
    optional { 
        ?lentry ddf:hasItemAboutEtymology ?etymN .
        ?etymN sioc:content ?etym .} .
    #infos pouvant être dupliquées pour une même lexicalEntry
	optional {?lentry ddf:hasGender ?g }    
    optional {?lentry ddf:hasNumber ?n }
    optional { ?lentry ddf:hasTransitivity ?tran }
    optional { ?lentry ddf:hasPartOfSpeech ?p }
    # Détails sur le LexicalSense
    ?lentry ontolex:sense ?sense .
    optional{ ?sense skos:definition ?def . }
    optional{ ?sense ddf:hasRegister ?reg } .
    optional{ ?sense ddf:hasTemporality ?temp } .
    optional{ ?sense ddf:hasConnotation ?con } .
    optional{ ?sense ddf:hasSociolect ?soc } .
    optional{ ?sense ddf:hasDomain ?dom } .
    optional{ ?sense ddf:hasItemAboutSense ?senseItemN .
        ?senseItemN sioc:content ?senseItem} .
    optional{ ?sense lexicog:usageExample ?usex .
    	optional{ ?usex dct:bibliographicalCitation ?ref . }
    	optional{ ?usex rdf:value ?ex .  }} .
    optional {?sense ddf:hasLocalisation ?loc }    
    optional { ?sense ddf:hasGlossary ?gloss } 
    # relations sense-entry
    optional { 
        ?sense ddf:lexicalSenseHasSemanticRelationWith ?sr .
        ?sr ddf:hasSemanticRelationType ?srel .
        optional{
        	?sr ddf:semanticRelationOfEntry ?entryRel .
        	?entryRel lexicog:describes ?lentryRel .
        	?lentryRel ontolex:canonicalForm ?formRel .
        	?formRel ontolex:writtenRep ?relatedForm .
    	}}
	}
LIMIT 10
``` 
