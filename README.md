### DICO FR

## inventaire-data

Fichiers `.txt`, `html`, `xml` de l'Inventaire des Particularités Lexicales du Français en Afrique Noire. Fichier `.graphml` pour représenter les exemples d'annotations fournis.

## pre-processing

Programme scala avec une entrée ligne de commande qui permet de faire du prétraitrement sur le PDF de l'Inventaire. Il permet de récupérer le contenu textuel du PDF. De convertir ce contenu textuel en un fichier XML

## vocabulaires-controlés

Fichiers `.ttl` contenant les vocabulaires controlés.

## ontologie-ddf-owl

Ontologie
