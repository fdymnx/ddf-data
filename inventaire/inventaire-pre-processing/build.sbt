import sbt.Opts.resolver
import sbt.url


lazy val preprocessing = (project in file(".")).
	settings(
		inThisBuild(List(
			name := "pre-processing",
			organization := "com.mnemotix",
			scalaVersion := "2.12.8",
			homepage := Some(url("http://www.mnemotix.com")),
			scalacOptions ++= Seq("-encoding", "utf8", "-Xlint", "-Ybreak-cycles"),
			isSnapshot := true,
			pomIncludeRepository := { _ => false },
			licenses := Seq("Apache 2.0" -> url("https://opensource.org/licenses/Apache-2.0")),
			scmInfo := Some(
				ScmInfo(
					url("https://bitbucket.org/mnemotix/lifty-cli.git"),
					"scm:git@bitbucket.org:mnemotix/lifty-cli.git"
				)
			),
			developers := List(
				Developer(
					id = "prlherisson",
					name = "Pierre-René Lhérison",
					email = "pr.lherisson@mnemotix.com",
					url = url("http://www.mnemotix.com")
				)
			),
			publishArtifact := true,
			publishMavenStyle := true,
			publishTo in ThisBuild := {
				val nexus = "https://repo.mnemotix.com/repository"
				if (isSnapshot.value) Some("MNX Nexus" at nexus + "/maven-snapshots/")
				else Some("MNX Nexus" at nexus + "/maven-releases/")
			},
			credentials += Credentials(Path.userHome / ".ivy2" / ".credentials"),
			resolvers ++= Seq(
				resolver.mavenLocalFile,
				Resolver.mavenLocal,
				"MNX Nexus (releases)" at "https://repo.mnemotix.com/repository/maven-releases/",
				"MNX Nexus (snapshots)" at "https://repo.mnemotix.com/repository/maven-snapshots/",
				"scalaz-bintray" at "http://dl.bintray.com/scalaz/releases",
				"Sonatype (Snapshots)" at "https://oss.sonatype.org/content/repositories/snapshots",
				"Typesafe (Releases)" at "http://repo.typesafe.com/typesafe/releases/",
				"DataStax Repo" at "https://repo.datastax.com/public-repos/",
				"jitpack" at "https://jitpack.io"
			),
			updateOptions := updateOptions.value.withGigahorse(false)
		)),
		libraryDependencies ++= Dependencies.common,

		assemblyMergeStrategy in assembly := {
			case PathList("META-INF", xs @ _*) => MergeStrategy.discard
			case x => MergeStrategy.first
		}
	)

 