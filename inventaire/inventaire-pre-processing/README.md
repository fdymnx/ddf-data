# pre-processing

Pre-processing est une application qui permet d'automatiser certaines tache de prétraitement.

## compilation

Pour obtenir le Fat jar : `sbt assembly`.

Il sera publié au chemin suivant : `target/scala-2.12/preprocessing-assembly-0.1.0-SNAPSHOT.jar`

## utilisation

Pour extraire le contenu textuel du pdf :

`java -cp target/scala-2.12/preprocessing-assembly-0.1.0-SNAPSHOT.jar org.dictionnairedesfrancophones.services.InventaireTxtExtraction -f le_fichier_de_linventaire.pdf -o /chemin/repertoire/de/sauvegarde`

Les fichiers en sorti :

- `rawInvenraire.txt` le contenu textuel
- `wordsFromAlgo.txt` les entrées du dictionnaire
- `referencesFromAlgo.txt` les références, la bibliographie

Pour sauvegarder sous format xml :

`java -cp target/scala-2.12/preprocessing-assembly-0.1.0-SNAPSHOT.jar org.dictionnairedesfrancophones.services.XMLConversion -f le_fichier_de_linventaire.txt -o /chemin/repertoire/de/sauvegarde`

