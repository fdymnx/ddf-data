package org.dictionnairedesfrancophones.preprocessing

import org.scalatest.{FlatSpec, Matchers}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class DicoPreprocessingSpec extends FlatSpec with Matchers {
  behavior of "GraphIndexerConfig"

  val nameTestFile = "/definitionTest.txt"
  val nameTest = getClass.getResource(nameTestFile).getFile

  val dicoProcessing = new DicoPreprocessing()

  it should "read a file" in {
    val content = dicoProcessing.readFile(nameTest)
    content.isEmpty shouldBe false
  }

  it should "split the file in the good number of Resource" in {
    val content = dicoProcessing.readFile(nameTest)
    val result = dicoProcessing.splitByParagraph(content)
    result.size shouldEqual 26
  }

  it should "split the Resource in an Entry and a Definition" in {
    val resources = dicoProcessing.splitByParagraph(dicoProcessing.readFile(nameTest))
    val dicoEntry = resources.map(resource => dicoProcessing.dicoEntry(resource))
    dicoEntry.length shouldEqual 26
  }

  it should "it should remove the arabic number" in {
    val entry = "1. aba,"
    entry.replaceAll("[0-9]\\.", "").toLowerCase
  }

  val entry =
    """  I. prép. Employé par confusion avec « en » dans différentes cooccurrences. D 1 ' B.F., MA.,
      |NIG. died. En, durant, pendant. « A mon jeune âge, j'étais au village » (04). A mon absence : en
      |mon absence. A l'absence de : en l'absence de. A mon enfance : durant mon enfance. « A mon
      |enfance, je chassais les margouillats* » (13). A bas âge (B.F.) : en bas âge, durant la petite enfance.
      |D 2 ' B.F., MA., SEN. Accompagne les compléments de lieu consistant en désignations de rues,
      |places (sans prép. en fr. st.). «J'habite à la rue Carnot » (06). D 3 ' B.F. Sur. « Il ne laisse rien
      |à son passage» (13). D 4 ' B.F. A tous points : en tous points. « Void ma tête. Elle est à tous
      |points semblable à celle de notre regretté père » (135).
      |II. A PART DE.  V. part.
      |III.  V. à la douce, à l'incognito, à peine que, à plus, à toute.""".stripMargin

  val entryWithoutRomans =
    """
      | n.f. RW., ZA. D 1 ' Milice privée à la solde d'un politicien rebelle. « Au mois d'août,
      |Kisangani (ex. Stanleyville) est occupé par les "jeunesses' révolutionnaires avec l'appui de la
      |population locale (...) » (395, p. 65). D 2 ' Membre d'une rébellion, d'un mouvement rebelle,
      |anti-gouvernemental. «On a réussi à capturer plusieurs jeunesses » (11).
    """.stripMargin

  val entryWithIandII =
    """
      | I.  v. tr. dir. D 1 ' BE., MA., TCH., TO. (En parlant d 'un musicien), jouer de (+ compì, d'objet
      |désignant l'instrument). « Il joue bien la guitare ! » (10). D 2 ' SEN. (Dans le domaine sportif),
      |rencontrer, jouer contre. «La J.A. n'a pas joué le D.U.C. » (81, 03-03-1975).
      |II. JOUER SA VIE. loc. verb. CAM. Vivre sa vie, profiter de la vie. « Il a bien joué sa vie avant de
      |mourir » (12).
      |SYN. : manger la vie*.
    """.stripMargin

  it should "split entry from I to II" in {
    val iter = dicoProcessing.splitByI(entry)
    iter.get shouldEqual ("""prép. Employé par confusion avec « en » dans différentes cooccurrences. D 1 ' B.F., MA.,
                            |NIG. died. En, durant, pendant. « A mon jeune âge, j'étais au village » (04). A mon absence : en
                            |mon absence. A l'absence de : en l'absence de. A mon enfance : durant mon enfance. « A mon
                            |enfance, je chassais les margouillats* » (13). A bas âge (B.F.) : en bas âge, durant la petite enfance.
                            |D 2 ' B.F., MA., SEN. Accompagne les compléments de lieu consistant en désignations de rues,
                            |places (sans prép. en fr. st.). «J'habite à la rue Carnot » (06). D 3 ' B.F. Sur. « Il ne laisse rien
                            |à son passage» (13). D 4 ' B.F. A tous points : en tous points. « Void ma tête. Elle est à tous
                            |points semblable à celle de notre regretté père » (135).""".stripMargin)

    val iterNone = dicoProcessing.splitByI(entryWithoutRomans)
    iterNone shouldEqual (None)
  }

  it should "split entry from II to III" in {
    val iter = dicoProcessing.splitByIItoIII(entry)
    iter.get shouldEqual ("A PART DE.  V. part.")
    val iterNone = dicoProcessing.splitByIItoIII(entryWithoutRomans)
    iterNone shouldEqual (None)
  }

  val thorens = "A PART DE.  V. part."

  it should "retreive the Maj word" in {
    val thorens = "A PART DE.  V. part."
    val result = dicoProcessing.writtenRep(thorens)
    result.get shouldEqual ("A PART DE.")
  }

  it should "split entry from II to end of String" in {
    val iter = dicoProcessing.splitByIItoIII(entryWithIandII)
    iter.get shouldEqual ("""JOUER SA VIE. loc. verb. CAM. Vivre sa vie, profiter de la vie. « Il a bien joué sa vie avant de
                            |mourir » (12).
                            |SYN. : manger la vie*.""".stripMargin)
    val iterNone = dicoProcessing.splitByIItoIII(entryWithoutRomans)
    iterNone shouldEqual (None)
  }

  it should "split entry from III to end of string" in {
    val iter = dicoProcessing.splitByIIItoIV(entry)
    iter.get shouldEqual ("V. à la douce, à l'incognito, à peine que, à plus, à toute.")
    val iterNone = dicoProcessing.splitByIIItoIV(entryWithoutRomans)
    iterNone shouldEqual (None)
  }

  val entrySplit =
    """prép. Employé par confusion avec « en » dans différentes cooccurrences. D 1 ' B.F., MA.,
      |NIG. died. En, durant, pendant. « A mon jeune âge, j'étais au village » (04). A mon absence : en
      |mon absence. A l'absence de : en l'absence de. A mon enfance : durant mon enfance. « A mon
      |enfance, je chassais les margouillats* » (13). A bas âge (B.F.) : en bas âge, durant la petite enfance.
      |D 2 ' B.F., MA., SEN. Accompagne les compléments de lieu consistant en désignations de rues,
      |places (sans prép. en fr. st.). «J'habite à la rue Carnot » (06). D 3 ' B.F. Sur. « Il ne laisse rien
      |à son passage» (13). D 4 ' B.F. A tous points : en tous points. « Void ma tête. Elle est à tous
      |points semblable à celle de notre regretté père » (135).""".stripMargin

  val entrySplitWithoutD =
    """abba. n. TCH. oral. Employé comme terme d'adresse à l'égard d'un homme d'un certain
      |âge qui inspire le respect.
      |SYN. : m'ba , papa*.
    """.stripMargin

  it should "split entry split" in {
    val subdef = dicoProcessing.splitByB(entrySplit)
    subdef.size shouldEqual (5)
    dicoProcessing.splitByB(entrySplitWithoutD).size shouldEqual (1)
  }

  val entryWithLocalization =
    """B.F., MA.,
      |NIG. died. En, durant, pendant. « A mon jeune âge, j'étais au village » (04). A mon absence : en
      |mon absence. A l'absence de : en l'absence de. A mon enfance : durant mon enfance. « A mon
      |enfance, je chassais les margouillats* » (13). A bas âge (B.F.) : en bas âge, durant la petite enfance.""".stripMargin

  /*
  it should "recognize localization" in {
    val listOfCountry = dicoProcessing.miscellaneous(Miscellaneous.localisation,entryWithLocalization)
    listOfCountry.size shouldEqual(3)
  }
  */

  it should "not find connotation" in {
    val listOfConnotation = dicoProcessing.miscellaneous(Miscellaneous.connotation, entryWithLocalization)
    listOfConnotation.size shouldEqual (0)
  }

  it should "recognize citation" in {
    val listOfCitation = dicoProcessing.citation(entryWithLocalization)
    listOfCitation.size shouldEqual (2)
  }

  val entryWithPronounciation =
    """[abakos], abacost (RW., ZA.), abakos (CA.). (de l'expression fr. : A bas le costume, s. -ent.
      |européen, slogan zaïrois employé lors des campagnes de sensibilisation pour l'authenticité*).
      |n.m.
    """.stripMargin

  it should "recognize pronounciation" in {
    val pronounciation = dicoProcessing.phonetique(entryWithPronounciation)
    pronounciation.size shouldEqual (1)
    pronounciation(0).toString() shouldEqual ("[abakos]")
  }

  it should "split the definition in many words" in {
    val words = dicoProcessing.splitByword(entryWithPronounciation)
    words.size shouldEqual (28)
  }

  it should "compute de levenshtein distance" in {
    val words = dicoProcessing.splitByword(entryWithPronounciation)
    val values = dicoProcessing.otherForms(words, "ABACOS")
  }

  val myRegex = "CA\\.?".r

  it should "recognize my regex" in {
    val recog = myRegex.findAllMatchIn(entryWithPronounciation).toSeq
    recog.size shouldEqual (1)
    recog(0).toString() shouldEqual ("CA.")
  }


  it should "recognize the fist split as an lexical Info" in {
    val aDefinition =
      """n.f. BE., B.F., CA., CAM., CL, MA., NIG., RW., TCH., TO., ZA. D 1 ' Action visant à
        |donner un caractère plus africain à (une entreprise, un programme, un travail, un orga-
        |nisme,...). D 2 ' Action qui consiste à remplacer le personnel européen par du personnel
        |africain.  V. béninisation, bénisation, ivoinsation, malianisation, nigérianisation, nigérisa-
        |tion, sénégalisation, togolisation, burkinisation, zaïrianisation.
      """.stripMargin

    val ds = dicoProcessing.splitByB(aDefinition)
    dicoProcessing.islexicalInfo(ds(0)) shouldBe (true)
  }

  it should "extract the canonicalForm JOUER SA VIE the definition" in {
    val deMarne = " JOUER SA VIE. loc. verb. CAM. Vivre sa vie, profiter de la vie. « Il a bien joué sa vie avant de mourir "
    val result = dicoProcessing.canonicalForm(deMarne)
    result.get.trim shouldBe ("JOUER SA VIE.")
  }

  it should "not extract canonicalForm" in {
    val aDefinition =
      """n.f. BE., B.F., CA., CAM., CL, MA., NIG., RW., TCH., TO., ZA. D 1 ' Action visant à
        |donner un caractère plus africain à (une entreprise, un programme, un travail, un orga-
        |nisme,...). D 2 ' Action qui consiste à remplacer le personnel européen par du personnel
        |africain.  V. béninisation, bénisation, ivoinsation, malianisation, nigérianisation, nigérisa-
        |tion, sénégalisation, togolisation, burkinisation, zaïrianisation.
      """.stripMargin
    val result = dicoProcessing.canonicalForm(aDefinition)
    result.isEmpty shouldBe (true)
  }
}