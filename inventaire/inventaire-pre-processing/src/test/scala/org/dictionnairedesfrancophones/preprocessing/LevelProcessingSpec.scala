package org.dictionnairedesfrancophones.preprocessing

import org.scalatest.{FlatSpec, Matchers}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class LevelProcessingSpec  extends FlatSpec with Matchers {
  behavior of "Level Processing"

  val dicoProcessing = new DicoPreprocessing()

  it should "convert the last D to a Roman" in {
    val levelProcessing = new LevelProcessing(dicoProcessing)
    val aDefinition =
      """abrév. de beau-frère ou de beau-père, n.m. D 1 ' B.F., CA., C.I., MA., NIG., SEN., ZA. (fam.).
        |Beau-frère. « Ça y est ! Les contractions commencent ! Aïe ! Mon beau s'apprêtait à sortir (...) se
        |ravise, rejette sa chemise et regarde d'un air navré ma sœur, le ventre parfaitement bombé qu'est
        |en train d'pousser des cris » (80, n"206, 08-02-1978).
        |SYN. : bokilo*.
        |D 2 ' B.F., C.I., NIG., SEN. (oral, fam.). Beau-père. D 3 ' B.F. fam. plus rare. Beau-fils. D 4 ' CA.,
        |C.I., TCH. (dial.). Tout membre de la belle-famille, de sexe masculin. « Mon beau me réclame la
        |dot* » (09).
        |REM. En C.I., le mot est parfois appliqué à un membre féminin de la famille alliée (chez les
        |non-lettrés).
        |D 5 ' BEAUX n.m. plur. B.F. fam. Ensemble des membres masculins et féminins de la famille
        |du conjoint, belle-famille. «Je ne suis par revenu chez mes beaux depuis les funérailles* » (13).""".stripMargin
    val result = levelProcessing.toRomans(aDefinition)
    result.size shouldEqual(1)
  }

  it should "remove the recognized D" in {
    val levelProcessing = new LevelProcessing(dicoProcessing)
    val aDefinition =
      """abrév. de beau-frère ou de beau-père, n.m. D 1 ' B.F., CA., C.I., MA., NIG., SEN., ZA. (fam.).
        |Beau-frère. « Ça y est ! Les contractions commencent ! Aïe ! Mon beau s'apprêtait à sortir (...) se
        |ravise, rejette sa chemise et regarde d'un air navré ma sœur, le ventre parfaitement bombé qu'est
        |en train d'pousser des cris » (80, n"206, 08-02-1978).
        |SYN. : bokilo*.
        |D 2 ' B.F., C.I., NIG., SEN. (oral, fam.). Beau-père. D 3 ' B.F. fam. plus rare. Beau-fils. D 4 ' CA.,
        |C.I., TCH. (dial.). Tout membre de la belle-famille, de sexe masculin. « Mon beau me réclame la
        |dot* » (09).
        |REM. En C.I., le mot est parfois appliqué à un membre féminin de la famille alliée (chez les
        |non-lettrés).
        |D 5 ' BEAUX n.m. plur. B.F. fam. Ensemble des membres masculins et féminins de la famille
        |du conjoint, belle-famille. «Je ne suis par revenu chez mes beaux depuis les funérailles* » (13).""".stripMargin
    val result = levelProcessing.toRomans(aDefinition)
    val newDefinition = levelProcessing.replaceInRoman(aDefinition, result)
  }

  it should "remove the recgonize D and distribute the lexicalInfo in new" in {
    val levelProcessing = new LevelProcessing(dicoProcessing)
    val aDefinition =
      """abrév. de beau-frère ou de beau-père, n.m. D 1 ' B.F., CA., C.I., MA., NIG., SEN., ZA. (fam.).
        |Beau-frère. « Ça y est ! Les contractions commencent ! Aïe ! Mon beau s'apprêtait à sortir (...) se
        |ravise, rejette sa chemise et regarde d'un air navré ma sœur, le ventre parfaitement bombé qu'est
        |en train d'pousser des cris » (80, n"206, 08-02-1978).
        |SYN. : bokilo*.
        |D 2 ' B.F., C.I., NIG., SEN. (oral, fam.). Beau-père. D 3 ' B.F. fam. plus rare. Beau-fils. D 4 ' CA.,
        |C.I., TCH. (dial.). Tout membre de la belle-famille, de sexe masculin. « Mon beau me réclame la
        |dot* » (09).
        |REM. En C.I., le mot est parfois appliqué à un membre féminin de la famille alliée (chez les
        |non-lettrés).
        |D 5 ' BEAUX n.m. plur. B.F. fam. Ensemble des membres masculins et féminins de la famille
        |du conjoint, belle-famille. «Je ne suis par revenu chez mes beaux depuis les funérailles* » (13).""".stripMargin


    val result = levelProcessing.dsToRomans(aDefinition)

  }

  it should "not remove split more than once" in {
    val levelProcessing = new LevelProcessing(dicoProcessing)
    val aDefinition = """ n.m. BE., B.F., CA., C.I. , MA., NIG., SEN., TCH, TO, ZA. Cuisinier ; domestique chargé
                        |des travaux courants du ménage et en particulier de la cuisine.
                        |REM. Les domestiques sont classés par catégories en fonction de leur compétence. « Boy-cuisinier
                        |cinquième catégorie » (03).
                        |SYN. (part.) : cook*, pitshi*.""".stripMargin
    val result = levelProcessing.dsToRomans(aDefinition)
  }

  def romansToList(values: Option[String]*) = {
    values.collect {
      case v if v.isDefined => v.get
    }
  }

  it should "nfeunfujf" in {
    val levelProcessing = new LevelProcessing(dicoProcessing)
    val aDefinition = """[bití] (de l'angl.). n.m. ZA. D 1 ' Débarcadère d'un port fluvial. «Je dévalai la côte au
                        |galop et débouchai au beach au moment précis où s'amarrait le vapeur» (233, p . 190). D 2 ' Petite
                        |plage fluviale où les gens viennent se laver. « Mais j'aime surtout me diriger vers le fleuve. ]e
                        |m'immobilise sur le beach et j'écoute le clapotis incessant de l'eau léchant la berge. » (233, p. 119).
                        |D 3 ' Sur une étendue d'eau, point de passage par gué, par pirogue ou par bac. «Cette
                        |pirogue a chaviré en traversant le beach » (11). D 4 ' Ville basse, centre commercial. « Je descends
                        |au beach » [: je vais en ville] (11).
                        |ENCYCL. Le quatrième sens s'explique par la configuration géographique de certaines villes
                        |où l'on distingue le centre commercial situé le long du fleuve, le centre dit résidentiel situé
                        |sur le plateau et la cité*.""".stripMargin



    val result = levelProcessing.dsToRomans(aDefinition)

    //result.foreach(d => println(s"** $d **/"))
    val ds = dicoProcessing.splitByB(aDefinition)

    //ds.foreach(d => println(s"** $d **/"))

    ds.foreach { d =>
      println(dicoProcessing.canonicalForm(d))
    }

  }
}

/*
  def toRomans(roman: String): Seq[String] = {
    val ds = dicoProcessing.splitByB(roman)
    ds.filter { d =>
      dicoProcessing.canonicalForm(d).isDefined
    }
  }
 */