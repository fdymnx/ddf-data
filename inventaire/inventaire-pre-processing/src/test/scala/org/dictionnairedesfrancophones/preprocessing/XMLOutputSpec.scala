package org.dictionnairedesfrancophones.preprocessing

import java.io.File

import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}

import scala.reflect.io.Path
import scala.util.Try
import scala.xml.NodeSeq

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class XMLOutputSpec extends FlatSpec with Matchers with BeforeAndAfterAll {
  behavior of "XML Output"

  val directoryPath = "src/test/resources/data"

  override def beforeAll() {
    new File(directoryPath).mkdirs
    Thread.sleep(5000)
  }

  override def afterAll() {
    val path: Path = Path (directoryPath)
    Try(path.deleteRecursively())
    Thread.sleep(5000)
  }

  it should "read entries" in {
    val nameTestFile = "/definitionTest.txt"
    val nameTest = getClass.getResource(nameTestFile).getFile
    val xmlOuput = new XMLOutput("", nameTest)
    xmlOuput.entries.size shouldEqual(26)
  }

  it should "write entries in xml form in memory" in {
    val nameTestFile = "/definitionTest.txt"
    val nameTest = getClass.getResource(nameTestFile).getFile
    val xmlOuput = new XMLOutput("", nameTest)
    val xml = xmlOuput.write(xmlOuput.entries)
    val p = new scala.xml.PrettyPrinter(80, 4)
    println(p.format(xml))
    val entries = xml  \ "Entry"
    entries.size shouldEqual(26)
  }

  it should "write the file in resources" in {
    val nameTestFile = "/definitionTest.txt"
    val nameTest = getClass.getResource(nameTestFile).getFile
    val xmlOuput = new XMLOutput(directoryPath, nameTest)
    xmlOuput.dump(xmlOuput.write(xmlOuput.entries))
    getClass.getResource(s"$directoryPath/inventaire.xml")
  }
}
