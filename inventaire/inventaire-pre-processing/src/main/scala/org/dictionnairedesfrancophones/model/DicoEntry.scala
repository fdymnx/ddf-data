package org.dictionnairedesfrancophones.model

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
  *
  * case class qui représente a le Nom d'une entrée et une Définition
  *
  * par exemple :
  *
  * ```
  * entryName = À
  * definition =
  * I. prép. Employé par confusion avec « en » dans différentes cooccurrences. D 1 ' B.F., MA.,
  * NIG. died. En, durant, pendant. « A mon jeune âge, j'étais au village » (04). A mon absence : en
  * mon absence. A l'absence de : en l'absence de. A mon enfance : durant mon enfance. « A mon
  * enfance, je chassais les margouillats* » (13). A bas âge (B.F.) : en bas âge, durant la petite enfance.
  * D 2 ' B.F., MA., SEN. Accompagne les compléments de lieu consistant en désignations de rues,
  * places (sans prép. en fr. st.). «J'habite à la rue Carnot » (06). D 3 ' B.F. Sur. « Il ne laisse rien
  * à son passage» (13). D 4 ' B.F. A tous points : en tous points. « Void ma tête. Elle est à tous
  * points semblable à celle de notre regretté père » (135).
  * II. A PART DE.  V. part.
  * III.  V. à la douce, à l'incognito, à peine que, à plus, à toute.
  * ```
  *
  * @param entryName le mot qu'une personne chercherait dans n'importe quel dictionnaire
  * @param definition le texte écrit à côté de l'entrée dans le dictionnaire
  *
  * @author Pierre-René Lhérisson
  *
  */

case class DicoEntry(entryName: String, definition: String)
