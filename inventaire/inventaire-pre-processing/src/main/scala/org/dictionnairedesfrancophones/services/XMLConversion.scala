package org.dictionnairedesfrancophones.services

import java.io.File

import com.typesafe.scalalogging.LazyLogging
import org.dictionnairedesfrancophones.model.XMLConversionConfig
import org.dictionnairedesfrancophones.preprocessing.XMLOutput

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object XMLConversion extends App with LazyLogging {

  val parser = new scopt.OptionParser[XMLConversionConfig]("XMLConversion") {

    override def showUsageOnError: Boolean = true

    head("InventaireTxtExtraction", "SNAPSHOT-0-1-0")

    opt[String]('o', "output").required().action((x,c)=> c.copy(outputFolder = x)).text("répertoire de sortie du fichier généré")
    opt[File]('f', "file").required().valueName("<file>").action((x,c)=> c.copy(txtFile = x)).text("fichier contenant les définitions")
  }

  parser.parse(args, XMLConversionConfig()) match {
    case Some(config) =>
      val output = config.outputFolder
      val txt = config.txtFile

      val xmlOutput = new XMLOutput(output, txt.getPath)
      xmlOutput.dump(xmlOutput.write(xmlOutput.entries))

    case None =>
      logger.debug("Erreur au niveau des arguments")
  }
}
