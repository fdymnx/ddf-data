package org.dictionnairedesfrancophones.services

import java.io.{BufferedWriter, File, FileWriter}

import com.typesafe.scalalogging.LazyLogging
import org.dictionnairedesfrancophones.model.InventaireTxtExtractionConfig
import org.dictionnairedesfrancophones.preprocessing.{PDFExtractor, TxtPreprocessing}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
  * CLI pour extraire le contenu textuel du PDF de l'Inventaire
  *
  *  @author Pierre-René Lhérisson
  */


object InventaireTxtExtraction extends App with LazyLogging  {

  val parser = new scopt.OptionParser[InventaireTxtExtractionConfig]("InventaireTxtExtraction") {

    override def showUsageOnError: Boolean = true

    head("InventaireTxtExtraction", "SNAPSHOT-0-1-0")

    opt[String]('o', "output").required().action((x,c)=> c.copy(outputFolder = x)).text("répertoire de sortie du fichier généré ")
    opt[File]('f', "file").required().valueName("<file>").action((x,c)=> c.copy(pdfFile = x)).text("fichier pdf de l'inventaire")
  }

  parser.parse(args, InventaireTxtExtractionConfig()) match {
    case Some(config) =>
      val output = config.outputFolder
      val pdfFile = config.pdfFile

      val dicoFile = new File(s"$output/rawInvenraire.txt")
      val bw = new BufferedWriter(new FileWriter(dicoFile))

      val pdfExtractor = new PDFExtractor(pdfFile)
      val handler = pdfExtractor.pdfTikaParse
      bw.write(handler.toString)
      bw.close()

      val txtPreprocessing = new TxtPreprocessing(output, dicoFile)
      txtPreprocessing.definitionsFromDico
      txtPreprocessing.references

    case None =>
      logger.debug("Erreur au niveau des arguments")
  }
}