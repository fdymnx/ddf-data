package org.dictionnairedesfrancophones.preprocessing

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class LevelProcessing(dicoProcessing: DicoPreprocessing) {

  def romansDstoRomans(romansList: Seq[String])  = {
    val newRomansInterm: Seq[String] = romansList.flatMap{ roman =>
     toRomans(roman)
    }
    val intermediate = romansList.map(roman => replaceInRoman(roman, newRomansInterm))
    //val newRomans  = romansList.flatMap(roman => distributeInRoman(roman, newRomansInterm))
    intermediate ++ newRomansInterm
  }

  def dsToRomans(ds: String): Seq[String] = {
    val newRoman = toRomans(ds)
    //val distributedinNewRoman = distributeInRoman(ds,newRoman)
    val roman = replaceInRoman(ds, newRoman)
    Seq(roman) ++ newRoman
  }

  def toRomans(roman: String): Seq[String] = {
    val ds = dicoProcessing.splitByB(roman)
    ds.filter { d =>
      dicoProcessing.canonicalForm(d).isDefined
    }
  }

  def replaceInRoman(roman: String, newRomans: Seq[String]): String = {
    newRomans.foldLeft(roman) { (s, r) =>
      s.replace(r, "")
    }
  }

  def distributeInRoman(roman: String, newRomans: Seq[String]) = {
    val ds = dicoProcessing.splitByB(roman)
    if (dicoProcessing.islexicalInfo(ds(0))) {
      newRomans.map(newRoman => s"${ds(0)} $newRoman")
    }
    else newRomans
  }
}