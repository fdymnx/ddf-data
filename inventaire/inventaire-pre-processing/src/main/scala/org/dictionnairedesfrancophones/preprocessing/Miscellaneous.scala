package org.dictionnairedesfrancophones.preprocessing

import scala.util.matching.Regex

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
  * un ensemble d'informations que nous pouvons extraire du texte
  *
  * @author Pierre-René Lhérisson
  */

object Miscellaneous {

  val localisation = Seq("""BE\\.?""", """B\\.F\\.""", """CA\\.?""", """CAM\\.?""", """C\\.I\\.""", """MA\\.?""", """NIG\\.?""", """RW\\.?""", """SEN\\.?""",
    """TCH\\.?""", """TO\\.?""", """ZA\\.?""")
  val localisationList = Seq("BE.", "B.F.", "CA.", "CAM.", "C.I.", "MA.", "NIG.", "RW.", "SEN.","TCH.", "TO.", "ZA.")

  val number = Seq("""s\\.?""", """plur\\.?""","""coll\\.\\set\\splur\\.?""","""inv\\.""")
  val numberList = Seq("s.", "plur.","coll. et plur.","inv.")

  val transitivity = Seq("""tr\\.?""", """intr\\.?""", """pronom\\.?""", """pass\\.?""")
  val transitivityList = Seq("tr.", "intr.", "pronom.", "pass.")

  val textualGenre = Seq("""littér\\.?""", """rech\\.?""", """oral\\.?""", """trad\\.?""")
  val textualGenreList = Seq("littér.", "rech.", "oral.", "trad.")

  val temporality = Seq("""ancient\\.""", """vieilli""", """vx\\.""", """néol\\.""", """ép\\.scolon\\.""")
  val temporalityList = Seq("ancient.", "vieilli", "vx.", "néol.", "ép colon.")

  val connotation = Seq("""péj\\.?""", """mélior\\.?""", """connot\\.?\\smélior\\.?""","""plais\\.?""", """non\\spéj\\.?""", """sans\\sconnot\\.?""",
    """sans\\sconnot\\.?\\sde\\sregistre""","""iron\\.?""")
  val connotationList = Seq("péj.", "mélior.", "connot. mélior.","plais.", "non péj.", "sans connot.",
    "sans connot. de registre","iron.")

  val domain = Seq("""hist\\.?""" ,"""ling\\.?""","""polit\\.?""")
  val domainList = Seq("hist." ,"ling.","polit.")


  val frequency = Seq("""cour\\.?""", """rare""")
  val frequencyList = Seq("cour.", "rare")

  val gender = Seq("""f\\.?""", """m\\.?""",  """m\\.?\\sf\\.?""")
  val genderList = Seq("f.", "m.",  "m. f.")

  val grammaticalConstraint = Seq("""Au\\spassif\\.""", """absolt\\.""", """employé\\sabsolt\\.""")
  val grammaticalConstraintList = Seq("Au passif.", "absolt.", "employé absolt.")


  val multiwordtype = Seq("""loc\\.?""", """loc\\.?\\sadj\\.?""", """loc\\.?\\snom\\.?""", """loc\\.?\\sprép\\.?""",
    """loc\\.?\\sverb\\.?""", """loc\\.?\\sadv\\.?""", """loc\\.?\\spronom\\.?""",
    """loc\\.?\\sconj\\.?""", """loc\\.?\\snom\\.?\\sexclam\\.?""",
    """express\\.?""", """loc\\.?\\sinterrog\\.?""")
  val multiwordtypeList = Seq("loc.", "loc. adj.", "loc. nom.", "loc. prép.",
    "loc. verb.", "loc. adv.", "loc. pronom.",
    "loc. conj.", "loc. nom. exclam.",
    "express.", "loc. interrog.")

  val sociolect = Seq("""arg\\.?""", """arg\\.?\\nscol\\.?""", """arg\\.?\\nétud\\.?""", """lang\\.?\\nétud\\.?"""	)
  val sociolectList = Seq("arg.", "arg. scol.", "arg. étud.", "lang. étud.")

  val senseRelation = Seq("""COMP\\.?\\set\\sSYN\\.?""", """SYN\\.?""", """ANT\\.?""", """ANTON\\.?""", """SYN\\.?\\s(part\\.)""", """spécialt\\.?""", """V\\.?""", """COMP\\.?"""
     , """DER\\.?""", """au\\.?\\sfig\\.?""", """par\\sext\\.?""")
  val senseRelationList = Seq("COMP. et SYN.", "SYN.", "ANT.", "ANTON.", "SYN. (part.)", "spécialt.", "V.", "COMP.", "DER.", "au. fig.", "par ext.")

  val regis = Seq("""fam\\.?""", """dial\\.?""", """pop\\.?""", """vulg\\.?""", """écrit\\.?""", """cour\\.?""", """oral\\.?""", """trad\\.?""")
  val regisList = Seq("fam.", "dial.", "pop.", "vulg;", "écrit.", "cour.", "oral.", "trad.")

  val partOfSpeech = Seq("""v\\.?""", """interj\\.?""", """onomat\\.?""", """adv\\.?""", """n\\.""", """n\\.\\spr\\.?""", """prép\\.?""", """pron\\.?""",
                        """conj\\.?""", """art\\.?""","""dém\\.?""","""adj\\.?""")
  val partOfSpeechList = Seq("v.", "interj.", "onomat.", "adv.", "n.", "n. pr.", "prép.", "pron.", "conj.", "art.","dém.","adj.")
}
