package org.dictionnairedesfrancophones.preprocessing

import org.dictionnairedesfrancophones.model.{DicoEntry, LiftyResource}
import org.dictionnairedesfrancophones.preprocessing
import org.dictionnairedesfrancophones.utils.Levenshtein

import scala.util.matching.Regex

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
  * DicoPreprocessing contient des processus d'extraction d'informations de l'Inventaire
  *
  * @author Pierre-René Lhérisson
  */

class DicoPreprocessing {

  /**
    * readFile lit le fichier et retourne un String contenant le texte
    *
    * @param name le nom du fichier
    * @return un String avec les espaces et les sauts de ligne
    */

  def readFile(name: String): String = {
    val source = scala.io.Source.fromFile(name)
    source.getLines mkString "\n"
  }

  /**
    *
    * splitByParagraph permet de séparer le String lu en LiftyResource
    *
    * @param lines String contenant le texte
    * @return une Liste de LiftyResource
    */

  def splitByParagraph(lines: String) = {
    lines.split("\n{2,}").map(par => new LiftyResource(par))
  }

  /**
    * convertit une LiftyResource en un DicoEntry en retrouvant les mots en majuscule
    *
    * @param resource LiftyResource
    * @return a DicoEntry
    */

  def dicoEntry(resource: LiftyResource): DicoEntry = {
    val entry = "([0-9].\\s)?(([A-ZÀ-ÜŒ.',()\\-]+)\\s)+".r.findFirstMatchIn(resource.wholeDef).mkString
    val definition = resource.wholeDef.replace(entry, "")
    DicoEntry(entry, definition)
  }

  def writtenRep(definition: String): Option[String] = {
    val regexMatch = "^(([0-9].\\s)?(([A-ZÀ-ÜŒ.',()\\-]+)\\s))+".r.findFirstMatchIn(definition)

    if (regexMatch.nonEmpty) {
      Some(regexMatch.get.toString().replaceAll("[0-9]\\.", "").trim)
    }
    else None
  }

  // split by I II III

  /**
    * Extrait les définitions qui commencent par un I
    *
    * @param definition la définition sans l'entrée
    * @return la définition comprise entre I et II si elle existe
    */

  def splitByI(definition: String): Option[String]  = {
    val regexMatch = "(I\\.\\s)((\\s)?([A-ZÀ-ÜŒa-zà-üœ0-9.',()\\-«»:*]+)\\s)+(II\\.)".r.findFirstMatchIn(definition)

    if (regexMatch.nonEmpty) {
      Some(regexMatch.get.toString().replace("II.", "").replace("I.", "").trim)
    }
    else None
  }

  /**
    * Extrait les définitions qui commencent par un II
    *
    * @param definition la définition sans l'entrée
    * @return la définition comprise entre II et la fin de la définition s'il n'y a pas d'autres blocs si elle existe
    */

  def splitByIItoEnd(definition: String): Option[String] = {
    val regexMatch =  "(II\\.\\s)((\\s)?([A-ZÀ-ÜŒa-zà-üœ0-9.',()\\-«»:*$]+)\\s)+[A-ZÀ-ÜŒa-zà-üœ0-9.',()\\-«»:*$]+".r.findFirstMatchIn(definition)
    if (regexMatch.nonEmpty) {
      Some(regexMatch.get.toString().replace("II.","").trim)
    }
    else None
  }

  /**
    * Extrait les définitions qui commencent par un II
    *
    * @param definition la définition sans l'entrée
    * @return la definition comprise entre II et III si elle existe
    */

  def splitByIItoIII(definition: String): Option[String] = {
    if (!definition.contains("III")) {
      splitByIItoEnd(definition)
    }
    else {
      val regexMatch = "(II\\.\\s)((\\s)?([A-ZÀ-ÜŒa-zà-üœ0-9.',()\\-«»:*]+)\\s)+(III\\.)".r.findFirstMatchIn(definition)
      if (regexMatch.nonEmpty) {
        Some(regexMatch.get.toString().replace("III.", "").replace("II.", "").trim)
      }
      else None
    }
  }

  /**
    * Extrait les définitions qui commencent par un III
    *
    * @param definition la définition sans l'entrée
    * @return la definition comprise entre III et et la fin de la définition s'il n'y a pas d'autres blocs si elle existe
    */

  def splitByIII(definition: String): Option[String] = {
    // start working
    val regexMatch =  "(III\\.\\s)((\\s)?([A-ZÀ-ÜŒa-zà-üœ0-9.',()\\-«»:*$]+)\\s)+[A-ZÀ-ÜŒa-zà-üœ0-9.',()\\-«»:*$]+".r.findFirstMatchIn(definition)

    if (regexMatch.nonEmpty) {
      Some(regexMatch.get.toString().replace("III.","").trim)
    }
    else None
  }

  /**
    * Extrait les définitions qui commencent par un III
    *
    * @param definition la définition sans l'entrée
    * @return la definition comprise entre III et IV si elle existe
    */


  def splitByIIItoIV(definition: String): Option[String] = {
    if (!definition.contains("IV")) {
      splitByIII(definition)
    }
    else {
      val regexMatch = "(III\\.\\s)((\\s)?([A-ZÀ-ÜŒa-zà-üœ0-9.',()\\-«»:*]+)\\s)+(IV\\.)".r.findFirstMatchIn(definition)

      if (regexMatch.nonEmpty) {
        Some(regexMatch.get.toString().replace("III.", "").replace("IV.", "").trim)
      }
      else None
    }
  }

  /**
    * Extrait les définitions qui commencent par un IV
    *
    * @param definition la définition sans l'entrée
    * @return la definition comprise entre IV et la fin de la définition si elle existe
    */

  def splitByIV(definition: String): Option[String] = {
    // start working
    val regexMatch =  "(IV\\.\\s)((\\s)?([A-ZÀ-ÜŒa-zà-üœ0-9.',()\\-«»:*$]+)\\s)+[A-ZÀ-ÜŒa-zà-üœ0-9.',()\\-«»:*$]+".r.findFirstMatchIn(definition)

    if (regexMatch.nonEmpty) {
      Some(regexMatch.get.toString().replace("IV.","").trim)
    }
    else None
  }

  /**
    * Extrait les définitions qui commencent avec un petit carré dans le pdf original
    *
    * @param definition une définition commençant par un I|II|III|IV sans l'entrée et sans le chiffre Romain
    * @return une liste de définition
    */

  def splitByB(definition: String) = {
    definition.split("D [0-9] '").toSeq
  }

  /**
    * Extrait les citations
    *
    * @param definition une definition sans l'entrée
    * @return une Liste de Regex.Match des citations
    */

  def citation(definition: String) = {
    "(«)((\\s)?([A-ZÀ-ÜŒa-zà-üœ0-9.',()\\-:*]+)\\s)+(»)".r.findAllMatchIn(definition).toSeq
  }

  /**
    * Extrait les phonétiques
    *
    * @param definition une definition sans l'entrée
    * @return une Liste de Regex.Match des informations sur la phonetique
    */

  def phonetique(definition: String) = {
    "\\[[A-ZÀ-ÜŒa-zà-üœ0-9.',()\\-«»:*]+\\]".r.findAllMatchIn(definition).toSeq
  }

  /**
    *
    * Extrait les informations diverses Miscellaneous
    *
    * @param listofSomething une liste de Miscellaneous
    * @param definition une définition sans l'entrée
    * @return la liste de Miscellaneous reconnue
    */

  def miscellaneous(listofSomething: Seq[String], definition: String): Seq[String] = {
    listofSomething.collect {
      case something if definition.contains(something) => something
    }
  }

  def splitByword(definition: String) = {
    definition.split("\\s").toSeq
  }

  def otherForms(words: Seq[String], baseWord:String) = {
    //words.forall(word => Levenshtein.levenshtein(word,baseWord)(word.length, baseWord.length))
    words.collect {
      case w if Levenshtein.levenshtein(w, baseWord)(w.length, baseWord.length) < 7 => w
    }
  }

  def regexPhonetique = "\\[[A-ZÀ-ÜŒa-zà-üœ0-9.',()\\-«»:*]+\\]"

  def regexDuDe = "\\(du|de\\s\\[A-ZÀ-ÜŒa-zà-üœ0-9.',()\\-«»:*]+)"

  val listOfPossibleEnding = Miscellaneous.localisationList ++ Miscellaneous.genderList  ++ Miscellaneous.textualGenreList ++
    Miscellaneous.domainList ++ Miscellaneous.senseRelationList ++ Seq("].")

  def islexicalInfo(definition:String) = {
    listOfPossibleEnding.map(pos => definition.trim.endsWith(pos)).contains(true)
  }

  def canonicalForm(definition:String) = {
    val otherForm = "([0-9].\\s)?(([A-ZÀ-ÜŒ.',()\\-]+)\\s)+".r.findFirstMatchIn(definition).mkString.trim
    if (listOfPossibleEnding.map(pos => otherForm.contains(pos)).contains(true) | otherForm == "." | otherForm == "," | otherForm == ")." | otherForm == "-"
      | otherForm == "" | otherForm == ".," | otherForm == ")," | otherForm == ".)," | otherForm == ".).") None
    else Some(otherForm)
  }

  def splitLexicalInfo(definition: String)  = {
   val regex: Regex = s"""$regexPhonetique?,?\\s?([A-ZÀ-ÜŒa-zà-üœ0-9.],?\\s?)*
      ${Miscellaneous.partOfSpeech.mkString("|")}?\\s?${Miscellaneous.gender.mkString("|")}?\\s" +
      s"${Miscellaneous.number.mkString("|")}?\\s(${Miscellaneous.localisation.mkString("|")}\\s)*" +
      s"${Miscellaneous.textualGenre.mkString("|")}?\\s${Miscellaneous.domain.mkString("|")}?""".r
    regex.findAllMatchIn(definition).toSeq
  }


}
