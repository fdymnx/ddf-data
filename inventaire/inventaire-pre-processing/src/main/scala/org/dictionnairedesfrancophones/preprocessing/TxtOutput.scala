package org.dictionnairedesfrancophones.preprocessing

import org.dictionnairedesfrancophones.model.DicoEntry

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class TxtOutput(directory: String, textFileDefinition: String) {

  val dicoProcessing = new DicoPreprocessing


  def entries = {
    dicoProcessing.splitByParagraph(dicoProcessing.readFile(textFileDefinition)).map {
      resource => dicoProcessing.dicoEntry(resource)
    }
  }

  def romansToList(values: Option[String]*) = {
    values.collect {
      case v if v.isDefined => v.get
    }
  }

  def write(entry: Seq[DicoEntry]): Unit = {
    entry.map {
      i => {
        val macroDefinition = i.definition
        val I = dicoProcessing.splitByI(macroDefinition)
        val II = dicoProcessing.splitByIItoIII(macroDefinition)
        val III = dicoProcessing.splitByIIItoIV(macroDefinition)
        val IV = dicoProcessing.splitByIV(macroDefinition)
        val romansList = romansToList(I,II,III,IV)
        if (romansList.size > 0) {

        }
        else {

        }
        s"${i.entryName} \n "
      }
    }
  }

  def writeDs(aDefinition: String) = {

      val ds = dicoProcessing.splitByB(aDefinition)
      ds map { d =>
          d.trim
      }
  }



}
