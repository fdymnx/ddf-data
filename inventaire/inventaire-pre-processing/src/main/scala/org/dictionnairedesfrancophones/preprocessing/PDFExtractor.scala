package org.dictionnairedesfrancophones.preprocessing

import java.io.{File, FileInputStream}

import org.apache.tika.metadata.Metadata
import org.apache.tika.parser.ParseContext
import org.apache.tika.parser.pdf.PDFParser
import org.apache.tika.sax.BodyContentHandler

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
  *
  * le PDFExtractor extrait le texte du pdf
  *
  * @param pdfFile le Fichier pdf
  *
  * @author Pierre-René Lhérisson
  */

class PDFExtractor(pdfFile: File) {

  /**
    * pdfTikaParse extrait le contenu textuel d'un pdf
    * @return un BodyContentHandler un objet interne de Tika
    */

    def pdfTikaParse = {
      val handler = new BodyContentHandler(-1)
      val metadata = new Metadata()
      val inputstream = new FileInputStream(pdfFile)
      val pcontext = new ParseContext()
      val pdfparser = new PDFParser()
      pdfparser.parse(inputstream, handler, metadata,pcontext)
      handler
    }
}
