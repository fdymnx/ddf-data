package org.dictionnairedesfrancophones.preprocessing

import java.io.{BufferedWriter, File, FileWriter}

import org.apache.commons.io.FileUtils
import org.dictionnairedesfrancophones.model.DicoEntry

import scala.xml.Elem

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
  *
  * Class pour obtenir une version pdf de l'Inventaire
  *
  * @param directory le repertoire ou sauvegarder les fichiers
  * @param textFileDefinition le fichier contenant les définitions
  *
  * @author Pierre-René Lhérisson
  */

class XMLOutput(directory: String, textFileDefinition: String) {

  val dicoProcessing = new DicoPreprocessing
  val levelProcessing = new LevelProcessing(dicoProcessing)

  /**
    * lit le fichier txt and retourne des DicoEntry
    * @return un array de DicoEntry
    */

  def entries = {
    dicoProcessing.splitByParagraph(dicoProcessing.readFile(textFileDefinition)).map {
      resource => dicoProcessing.dicoEntry(resource)
    }
  }

  def romansToList(values: Option[String]*) = {
    values.collect {
      case v if v.isDefined => v.get
    }
  }

  /**
    * Ecrit en mémoire un Fichier XML
    *
    * @param entry une Seq de DicoEntry
    * @return un fichier XML
    */

  def write(entry: Seq[DicoEntry]) = {
    <dictionnaire resource ="inv">
      {
      entry.zipWithIndex.map {
        case (e, i) => {
          println(e.entryName)
          <lexicog:Entry id={i.toString}>
            {<ontolex:Form>{e.entryName.replaceAll("[0-9]\\.", "").toLowerCase.trim}</ontolex:Form>}
              {
              val macroDefinition = e.definition
              val I = dicoProcessing.splitByI(macroDefinition)
              val II = dicoProcessing.splitByIItoIII(macroDefinition)
              val III = dicoProcessing.splitByIIItoIV(macroDefinition)
              val IV = dicoProcessing.splitByIV(macroDefinition)

              val romansList = romansToList(I,II,III,IV)

              if (romansList.size > 0) {
                  {
                    println("R")
                    specialcase(e.entryName)
                    //val newRomansList = levelProcessing.romansDstoRomans(romansList)
                    romansList.zipWithIndex.map {
                      case (roman, j) =>
                      <ontolex:LexicalEntry id={j.toString}>
                        {

                          {writeDs(roman)}


                        }
                      </ontolex:LexicalEntry>
                    }
                  }
              }
              else {





                if (dicoProcessing.splitByB(macroDefinition).size > 1) {
                  {
                    val newDefinition =  levelProcessing.dsToRomans(macroDefinition)
                    newDefinition.filter(p => p != "").zipWithIndex.map {
                      case (roman, j) =>
                        <ontolex:LexicalEntry id={j.toString}>
                          {
                          {writeDs(roman)}
                          }
                        </ontolex:LexicalEntry>
                    }
                  }
                }
                else {
                  <ontolex:LexicalEntry id ="0">
                    {
                    {writeDs(macroDefinition)}
                    }
                  </ontolex:LexicalEntry>
                }
              }
              }
          </lexicog:Entry>
        }
      }
      }
    </dictionnaire>
  }

  /**
    *
    * @param macroDefinition une définition
    * @return un sous arbre XML correspondant à un ensemble de champ
    */


  def writeDs(aDefinition: String) = {

    val ds = dicoProcessing.splitByB(aDefinition)
    if (dicoProcessing.islexicalInfo(ds(0))) {

      <lexicalInfo>{ds(0)}</lexicalInfo>
      <sense>
        {
        ds.zipWithIndex.collect {
          case (d, j) if (j != 0) => {
            <ontolex:LexicalSense id ={j.toString}>
              {
               d.trim.replaceAll("\\n", " ")
              }
            </ontolex:LexicalSense>
          }
         }
        }
      </sense>
    }

    else {
      <sense>
        {
        ds.zipWithIndex.map { case (d, j) =>
        <ontolex:LexicalSense id ={j.toString}>
          {
          d.trim.replaceAll("\\n", " ")
          }
        </ontolex:LexicalSense>
          }
        }
      </sense>
    }
  }

  /**
    * écrit le fichier XML sur disque
    *
    * @param dico le fichier XML en mémoire
    */

  def dump(dico: Elem) = {
    scala.xml.XML.save(s"$directory/inventaire.xml", dico, enc = "UTF-8",true, null)
    prettyDump(dico)
  }

  def prettyDump(dico: Elem) = {
    // 80 characters wide, 2 character indentation
    val prettyPrinter = new scala.xml.PrettyPrinter(80, 2)
    val prettyXml = prettyPrinter.format(dico)
    val file = new File(s"$directory/Prettyinventaire.xml")
    val bw = new BufferedWriter(new FileWriter(file))
    bw.write(prettyXml)
    bw.close()
  }

  def specialcase(ontolexForm: String) = {
    import java.io._
    val pw = new PrintWriter(new FileOutputStream(new File(s"$directory/ontolexFormSpecial.txt" ), true))
    pw.write(s"$ontolexForm \n")
    pw.close
  }
}
