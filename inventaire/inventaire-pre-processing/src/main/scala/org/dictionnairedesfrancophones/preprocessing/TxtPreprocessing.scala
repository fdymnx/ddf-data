package org.dictionnairedesfrancophones.preprocessing

import java.io.File
import java.nio.file.{Files, Paths}

/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
  * TxtPreprocessing extrait deux des principales partie de l'inventaire
  *
  * @param directory le chemin du répertoire ou nous allons sauvegarder les fichiers
  *
  * @author Pierre-René Lhérisson
  */

class TxtPreprocessing(directory: String,file: File) {

  /**
    * Élimine des lignes de la liste de ligne
    *
    * @param lines la liste des lignes du fichier
    * @param from ligne à partir de laquelle nous allons effacer
    * @param to ligne jusqu'à laquelle nous allons effacer
    * @return une Seq[String], les lignes que nous gardons
    */

  private def do2slice(lines: Seq[String], from: Int, to: Int): Seq[String] = {
    lines.slice(from, to)
  }

  /**
    *
    * @return écrit le fichier contenant les définitions
    */

  def definitionsFromDico = {
    val lines: Seq[String] = scala.io.Source.fromFile(file).getLines().toSeq
    val fileToWrite = new File(s"$directory/wordsFromAlgo.txt")
    Files.write(Paths.get(fileToWrite.getName), scala.collection.JavaConverters.asJavaCollection(do2slice(lines, 2414, 30202)))
  }

  /**
    *
    * @return écrit le fichier contenant les références
    */

  def references = {
    val lines: Seq[String] = scala.io.Source.fromFile(file).getLines().toSeq
    val fileToWrite = new File(s"$directory/referencesFromAlgo.txt")
    Files.write(Paths.get(fileToWrite.getName), scala.collection.JavaConverters.asJavaCollection(do2slice(lines, 30212, 32672)))
  }
}