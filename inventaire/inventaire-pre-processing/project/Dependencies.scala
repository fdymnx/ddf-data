import sbt._

object Version {
  lazy val scalaLogging = "3.9.0"
  lazy val scalaLangMod = "1.1.1"
  lazy val scalaTest = "3.0.5"
  lazy val scopt = "3.7.0"
  lazy val tika = "1.20"
}

object Library {
  lazy val scalaLogging = "com.typesafe.scala-logging" %% "scala-logging" % Version.scalaLogging
  lazy val scalaLangMod = "org.scala-lang.modules" %% "scala-xml" % Version.scalaLangMod
  lazy val scalaTest: ModuleID = "org.scalatest" %% "scalatest" % Version.scalaTest % Test
  lazy val scopt = "com.github.scopt" %% "scopt" % Version.scopt
  lazy val tikaCore = "org.apache.tika" % "tika-core" % Version.tika
  lazy val tikaParser = "org.apache.tika" % "tika-parsers" % Version.tika
}

object Dependencies {

  import Library._

  val common = List(scalaLogging, scalaLangMod, scalaTest, scopt, tikaCore, tikaParser)
}