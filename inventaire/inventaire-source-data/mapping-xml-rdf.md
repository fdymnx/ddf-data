# Mapping XML => RDF

# Notes de version

version courante = v0.5

## v0.5

Modification avec le nouveau schéma de mapping des URIs (cf https://docs.google.com/spreadsheets/d/1K4ctFsy53GH_j3Qc-QKhnf7gTUPaQSLux0P6k9XW7c8/edit#gid=1064238139)

## v0.4
Modification du modèle pour les notes liées à l'étymologie, et ajout de la capture de la phonétique et des formes alternatives; cf [##createLexicalEntry(...)](#createLexicalEntry(currentLexicogEntry, currentForm, currentLexicalEntry))

## v0.3

Prise en compte des informations contenues dans les lexical Sense, et parsing des semantic relations dans une 2è passe

## v0.2

Amélioration du code pour traiter les entrées simples à multiples définitions:

-   1 seul ontolex:LexicalEntry (pas de chiffres romains)
-   1 ou plusieurs définition
-   1 ou plusieurs exemples

## v0.1

Première version du mapping pour traiter les entrées simples, ie:

-   1 seul ontolex:LexicalEntry
-   1 définition
-   1 ou plusieurs exemples

et les champs suivants:

-   Forme cannonique
-   types de POS
-   définition
-   Exemple
-   localisation des:
    -   définition (sens)
    -   formes

# Exemples de cas d'entrées

## Cas simple:

Version texte

    KASSAK, kasak (du wolof). n.m. SEsfgfsfN. Ensemble de chants, danses, devinettes organisés le soir,
    pendant la période de circoncisi  on. « Les séances de kassak étaient tout autre chose que ce qu'elles
    sont maintenant, en fait l'occasion de faire entendre de beaux chants » (81, 19-09-1975). « Durant
    les nuits de clair de lune des kasak étaient organisés réunissant autour d'un grand feu de bois la
    jeunesse du quartier » (356, p. 23).
    NORME : inv. au plur.

Version XML

```xml
<lexicog:Entry id="2906">
  <ontolex:Form>kassak,</ontolex:Form>
  <ontolex:LexicalEntry id="0">
    <sense>
      <ontolex:LexicalSense id="0">
        kasak (du wolof). n.m. SEN. Ensemble de chants, danses, devinettes organisés le soir, pendant la période de circoncision. « Les séances de kassak étaient tout autre chose que ce qu'elles sont maintenant, en fait l'occasion de faire entendre de beaux chants » (81, 19-09-1975). « Durant les nuits de clair de lune des kasak étaient organisés réunissant autour d'un grand feu de bois la jeunesse du quartier » (356, p. 23). NORME : inv. au plur.
      </ontolex:LexicalSense>
    </sense>
  </ontolex:LexicalEntry>
</lexicog:Entry>
```

## Cas 1 seul entrée, plusieurs définitions

Texte

    BASSI, n.m. D 1 ' SEN. (Sorghum sp.). Graminée alimentaire. « Pour qu'Us poussent denses dans la
    plaine comme la souna* et le sagno* non comme le gros bassi des chevaux » (275, p. 71).
    SYN. : gros mil*, sorgho*.
    D 2 ' MA, NIG. (du haoussa, du zarma). Couscous de mil* ou de maïs consommé avec du
    sucre, des dattes, du lait (NIG.) ou avec une sauce (MA). D 3 ' SEN. Sauce d'arachide* qui
    accompagne le couscous. « Tu ne sais même pas préparer le bassi » (275, p. 19). Par melon., plat
    préparé avec cette sauce. « (Il) pensait à l'excellent bassi, ou au couscous ou au bon tô* (...) que
    Tara (...) allait pouvoir accomoderà (228, p. 51).
    COMP. : bassi-saleté*.

Version XML

```xml
<lexicog:Entry id="501">
  <ontolex:Form>bassi,</ontolex:Form>
  <ontolex:LexicalEntry id="0">
    <lexicalInfo>n.m. </lexicalInfo>
    <sense>
      <ontolex:LexicalSense id="1">
        SEN. (Sorghum sp.). Graminée alimentaire. « Pour qu'Us poussent denses dans la plaine comme la souna* et le sagno* non comme le gros bassi des chevaux » (275, p. 71). SYN. : gros mil*, sorgho*.
      </ontolex:LexicalSense>
      <ontolex:LexicalSense id="2">
        MA, NIG. (du haoussa, du zarma). Couscous de mil* ou de maïs consommé avec du sucre, des dattes, du lait (NIG.) ou avec une sauce (MA).
      </ontolex:LexicalSense>
      <ontolex:LexicalSense id="3">
        SEN. Sauce d'arachide* qui accompagne le couscous. « Tu ne sais même pas préparer le bassi » (275, p. 19). Par melon., plat préparé avec cette sauce. « (Il) pensait à l'excellent bassi, ou au couscous ou au bon tô* (...) que Tara (...) allait pouvoir accomoderà (228, p. 51). COMP. : bassi-saleté*.
      </ontolex:LexicalSense>
    </sense>
  </ontolex:LexicalEntry>
</lexicog:Entry>
```

## Exemple de relations sémantiques et notes dans le texte de la définition

Exemple avec 2 notes 'ENCYCL' et 'NORME' + 1 relation de synonyme
```xml
<lexicog:Entry id="1551">
  <ontolex:Form>cure-dents,</ontolex:Form>
  <ontolex:LexicalEntry id="0">
    <sense>
      <ontolex:LexicalSense id="0">
        cure dent [MA, TCH.]. n.m. B.F, CA, CI, MA, NIG, SEN, TCH. Bâtonnet de bois tendre utilisé pour se frotter et se brosser les dents et non pour les curer. « Les racines de Garcinia Afzelii (...) coupées à la taille d'un gros crayon, sont vendues sur tous les marchés comme cure-dents » (211). ENCYCL Son usage public est admis en dehors des repas. SYN. : bâtonnet dentaire*, frotte-dents*, sotiou*. NORME : appellation impr. pour frotte-dents*.
      </ontolex:LexicalSense>
    </sense>
  </ontolex:LexicalEntry>
</lexicog:Entry>
```

# Algorithme

```python
# 1ère passe
for currentLexicogEntry in list(lexicog:Entry):
  createLexicogEntry(currentLexicogEntry)
  currentForm = getNodeText(ontolex:Form)
  for currentLexicalEntry in list(ontolex:LexicalEntry):
    createLexicalEntry(currentLexicogEntry, currentForm, currentLexicalEntry)
    for currentLexicalSense in list(ontolex:LexicalSense):
      createLexicalSense(currentLexicogEntry, currentLexicalEntry, currentLexicalSense)
# 2ème passe pour les relations sémantiques
for currentLexicogEntry in list(lexicog:Entry):
    idr_last = 1 #incremental id of semantic relations, because URIs are based on lexicog:Entry URIS dinv:(n)/sr/(x)
    idr = idr_last
    for currentLexicalEntry in list(ontolex:LexicalEntry):
        idr = idr_last
        for currentLexicalSense in list(ontolex:LexicalSense):
            idr = idr_last # simple mechanism to keep track of the last relation id
            idr_last = createSemanticRelation(currentLexicogEntry, currentLexicalEntry, currentLexicalSense, idr)

```

## createLexicogEntry(currentLexicogEntry)

```python
createLexicogEntry(currentLexicogEntry):
  id = currentLexicogEntry['id']
  rdf = """
    ddfi:{{id}} a lexicog:Entry .
    """
  return(rdf)
```

## createLexicalEntry(currentLexicogEntry, currentForm, currentLexicalEntry)

```python
createLexicalEntry(currentLexicogEntry, currentForm,  currentLexicalEntry):
    rdf = ""
    id = currentLexicogEntry['id']
    ide = currentLexicalEntry['id']
    ts = datetime.now.timestamp()

    # récupération des infos lexicales: depuis le XML ou en parsant la définition
    lexicalInfo = currentLexicalEntry.xpath('//lexicalInfo/text()')
    if lexicalInfo None:
        senses = currentLexicalEntry.xpath('ontolex:LexicalSense')
        if len(senses) == 1:
            senseText = getNodeText(senses[0])
            lexicalInfo = extractLexicalInfo(senseText)
    # Determination du POSType, Localisations, et sous-classe de LexicalEntry avec la forme et les lexical infos
    # Autres infos lexicales qui ne sont pas des listes à détecter: transitivity, gender, number mais qui peuvent être nulles
    posType,
        transitivitytype,
        genderType,
        numberType,
        etymologyNote,
        canonicalFormPhonetic = parseLexicalInfo(lexicalInfo)

    # Alternative forms => renvoie une liste
    alternativeForms = getAlternativeForms(lexicalInfo)
    # pour la localisation on est dans un cas particulier où cette info est reliée à la forme alors qu'on l'a tire du texte du sens
    localisations = getLocalisations(lexicalInfo) # récupère une liste
    # 5 sous-classes à prévoir:  MultiWordExpression | Affix | (Word) > (Verb | InflectablePOS | InvariablePOS)
    # NB : on peut faire un dictionnaire de mapping entre le POSType et la sous-classe de Word à partir de la colonne G "dc:type" du tableau "part-of-speech-type" dans le fichier vocabulaire controllé
    lexicalEntryClass = getClassFromForm(currentForm, lexicalInfo)

    # On ajoute les infos lexicales
    rdf = """
    # la lexicog:Entry est reliée à une ontolex:LexicalEntry
    ddfi:entry#{{ts}} lexicog:describes ddfi:lexical-entry#{{ts}} ;

    # On spécifie ici la sous-classe de ontolex:LexicalEntry
    ddfi:lexical-entry#{{ts}} a ontolex:{{lexicalEntryClass}} ;
            ontolex:canonicalForm ddfi:form#{{ts}} ;

    # Formes canoniques avec phonétique
    ddfi:form#{{ts}} a ontolex:Form ;
      ontolex:writtenRep "{{currentForm}}"@fr ;
      ontolex:phoneticRep "{{canonicalFormPhonetic}}" .

    # Formes alternatives, l'incrément de la boucle est supposé démarrer à 1
    <% for form in {{alternativeForms}}%>
    ddfi:lexical-entry#{{ts}} ontolex:otherForm ddfi:form#{{ts}}{{loopIncrement}} .

    ddfi:form#{{ts}}{{loopIncrement}} a ontolex:Form ;
        ontolex:writtenRep "{{form}}"
    <%endfor%>

    # POS  relié à la LexicalEntry
    ddfi:lexical-entry#{{ts}} lexinfo:hasPartOfSpeech lexinfo:{{posType}} ;

    # Etymology modélisée comme un sioc:Post auquel on joint un score de "5"
    ddfi:lexical-entry#{{ts}} ddf:hasItemAboutEtymology ddfi:post#{{ts}} .

    dinv/{{id}}/en/{{ide}}/ety a sioc:Post ;
        sioc:content "{{etymologyNote}}" ;
        ddf:aboutEtymology ddfi:lexical-entry#{{ts}} .

    # A répéter pour chacune des 3 infos: ddf:hasTransitivity, ddf:hasGender, ddf:hasNumber mais qui peuvent être nulles
    <% if {{transitivityType}}%>
        ddfi:lexical-entry#{{ts}} ddf:hasTransitivity {{transitivityType.URI}}
    <% endif%>
    """
    # on ajoute la localisations à la forme
    # TBD dictionnaire de mapping
    for place in localisations:
        placeURI = placeMapping[place]
        rdf += """
        ddfi:form#{{ts}} ddf:formHasLocalisation {{placeURI}}
        """
    return(rdf)
```

## createLexicalSense(currentLexicogEntry, currentLexicalEntry, currentLexicalSense)

```python
createLexicalSense(currentLexicogEntry, currentLexicalEntry, currentLexicalSense):
  id = currentLexicogEntry['id']
  ide = currentLexicalEntry['id']
  ids = currentLexicalSense['id']
  ts = datetime.now.timestamp()
  rdf = """
    # On rattache le LexicalEntry (ici un ontolex:Word) à un LexicalSense
    ddfi:lexical-entry#{{ts}} ontolex:sense ddfi:lexical-entry#{{ts}} .
    dinv:{{id}}/en/{{ide}}/ls/{{ids}} a ontolex:LexicalSense .
  """
  # Parsing du textes; ex: n.m. BE., CA., C.I., SEN., TO., ZA. Fruit du corossolier*. « La marchande de fruits vend des corossols » (02). SYN. anone*, cœur de bœuf*.
  text = getNodeText(currentLexicalSense)
  # --- GENERALISATION ----
  # pour les informations suivantes, qu'on appeller "lexicalQualification" liées à des vocabulaires contrôlés, le fonctionnement est le même:
  # - on passe un filtre qui récupère l'info,
  # - et les triplets ont toujours la même forme

  # liste des lexicalQualification :: controlled vocabulary:
  # - ddf:hasLocalisation :: Place
  # - ddf:hasTemporality :: Temporality
  # - ddf:hasDomain :: Domain
  # - ddf:hasConnotation :: Connotation
  # - ddf:hasSociolect :: Sociolect
  # - etc.

  # exemples des localisations
  localisations = getLocalisations(senseText)
  for place in localisations:
    placeURI = placeMapping[place]
    rdf += """
      dinv:{{id}}/en/{{ide}}/ls/{{ids}} ddf:hasLocalisation {{placeURI}}
    """
  # Généralisation
  for type in qualificationTypes:
    qualification = getQualification(type, senseText)
    rdf+= """
      dinv:{{id}}/en/{{ide}}/ls/{{ids}} ddf:has{{type}} {{qualification}}
      """
  # Exemples sont un peu différents, et il faut parser 2 choses à la fois : le texte et la référence
  # Attention il peut y avoir plusieurs exemples d'usages
  examples = parseExamples(senseText)
  for i, example in enumerate(examples): #enumerate returns tuples with loop index and list item
    exampleText, referenceText = parseExample(example)
    rdf +=
    """
    dinv:{{id}}/en/{{ide}}/ls/{{ids}} lexicog:usageExample dinv:{{id}}/en/{{ide}}/ls/{{ids}}/ex/{{i}} ;

    dinv:{{id}}/en/{{ide}}/ls/{{ids}}/ex/{{i}} a lexicog:UsageExample ;
        rdf:value "{{exampleText}}"@fr ;
        dct:bibliographicalCitation "{{referenceText}}"@fr.
    """
  # Les sense Relation sont différentes également et doivent se faire dans une 2ème passe, car il faut capturer le type et l'entrée correspondante
  # Les notes et remarques: introduites par COM|ENCYCL|REM|NORME; il faut capturer simplement le contenu de la remarque, car elles sont toutes modélisés comme un sioc:Item relié au ontolex:LexicalSense
  notes = parseNotes(senseText) # voir plus bas une proposition de regex pour les notes
  for note in notes:
      idn = random(timestamp)
      # note = la string de texte de la note
      rdf+="""
      dinv:{{id}}/en/{{ide}}/ls/{{ids}} ddf:hasItemAboutSense dinv:item/{{idn}}.

      dinv:item/{{idn}} a sioc:Post;
        sioc:content "{{note}}"
      """

  # Le texte de la définition à la toute fin
  # Alternative A: on peut dépouiller progressivement senseText à chaque étape de reconnaissance; puis ce qui reste = définition => plus compliqué et difficile à faire évoluer
  # Alternative B: on peut simplement appliquer toute les regex les unes à la suite des autres pour retirer les parties de textes correspondantes, cf  parseDefinition(senseText) ci-après => moins efficace, mais p.e plus robuste
  definition = parseDefinition(senseText)
  rdf = """
    dinv:{{id}}/en/{{ide}}/ls/{{ids}} skos:definition "{{definition}}"@fr .
  """
  return rdf
```
## parseNotes

```python
parseNotes(senseText):
"""Vu qu'il n'y a pas de distinction des types de notes, on cherche à récupérer une liste des notes, chacune étant une simple string de texte"""
    regexNotes1 = " (COM\.|ENCYCL|REM\.)[^\:]*\:(.*?)(COMP|DER|SYN|ANT|ANTON)*)"
    regexNotes2 = "(NORME)[^\:]*\:(.*)(COMP|DER|SYN|ANT|ANTON)*""
    # TBD : voir comment récupérer la bonne valeur selon parser,
    liste = regexMatchAll(regexNotes1, senseText) # à priori $1
    liste += regexMatchAll(regexNotes2, senseText) #à priori $2
    return liste
```

## parseDefinition(senseText)

## Approche

Le texte de définition correspond à tout ce qui reste quand on a enlevé les informations parsables. On va donc
appliquer successivement les regex de parsing des infos contenus dans le texte de LexicalSense, pour retirer ces parties du texte de la LexcialSense:

-   les examples avec le numéro de référence. Regex : `(«[^»]{10,}»)\s*[^\(]*?(\([^\)]*\))*`
-   Les localisations :`((BE|B\.F|C\.A|CAM|C\.I|CA|MA|NIG|RW|SEN|TCH|THC|TO|ZA|CJ)[\.,]+)`
-   la connotation : `(mélior.|connot.|mélior.|péj.|plais.|iron)`
-   le registre : `(fam|dial|oral|pop|vulg|écrit|trad)[,\.]`
-   les relations sémantiques, type + renvois ou liste de termes : `(SYN|ANT|ANTON|COMP|DER|V)\.[^\:]*?\:(.*)` (TBD: à compléter avec tous les types de relation
-   les notes types "COM|ENCYCL|REM|NORME": `((COM\.|ENCYCL|REM\.)[^\:]*\:(.*?)(COMP|DER|SYN|ANT|ANTON)*)|((NORME)[^\:]*\:(.*)(COMP|DER|SYN|ANT|ANTON)*)`

### Exemples

Une définition peut:

-   être suivie d'un exemple ou pas
-   être constituée de 1 mot à 2 voire 3 phrases

2 phrases

    BIJOUTIER, 1ÈRE. n. SEN. Membre de la sous-caste des artisans du métal dont la profession
    traditionnelle était celle de bijoutier. Ainsi appelé même s'il exerce une autre profession.
    « Elles n'ont pas voulu monter dans la voiture de cet ingénieur, parce que c'était un bijoutier» (08).

sans exemples

    BILLETEUR, n.m. D 1 ' CA., MA., SEN. Agent administratif qui est chargé du billetage*. D 2 ' MA.
    Agent d'un service de transport chargé de vendre les titres de transport aux clients.

1 mot + exemple

    BOÎTE À MUSIQUE, n.f. MA., SEN. Electrophone. « C'est ainsi qu'à l'approche de l'hivernage*, on
    constate une recrudescence des postes de radio et de boîtes à musique » (134, 10-04-1979).
    SYN. : boîte*.

Ci-dessous une tentative de regex mais qui ne réussit pas à tout traiter.

```regex
(\s(BE|B\.F|C\.A|CAM|C\.I|CA|MA|NIG|RW|SEN|TCH|THC|TO|ZA)[\.,]+)(fam\.|dial\.|oral\.|pop\.|vulg\.|écrit\.|trad\.){0,1}(.*\.)(«[^»]*»){0,1}
```

## parseSemanticRelations

```python
createSemanticRelation(currentLexicogEntry, currentLexicalEntry, currentLexicalSense, idr):
"""
En utilisant une regex (par exemple:`(SYN|ANT)\.[^\:]*:([^(SYN|ANT|REM|ENCYCL|\n)]*)` --attention: à compléter avec tous les types de relation)
# Exemple de relations de Synonymie SYN. : médicament*, remède*.
"""
    # Needed for rebuilding URIS
    id = currentLexicogEntry['id']
    ide = currentLexicalEntry['id']
    ids = currentLexicalSense['id']
    relations = parseRelations(senseText) # on suppose récupérer une liste d'objets {relationType="", relatedTermsText=""}
    idr += 1 # to be incremented
    for rel in relations:
      relType = rel['relationType'] # ex: SYN
      relTypeURI = senseRelation[relType] # ex: SYN => http://data.dictionnairedesfrancophones.org/authority/sense-relation/synonym
      relTerms = split(',', rel['relatedTermsText']) # ex: médicament*, remède*.
      relatedEntriesURI = lookUpEntries(relTerms) # il faut retrouver les éventuelles entrées
      for entryURI in relatedEntriesURI:
          rdf += """
          dinv:{{id}}/en/{{ide}}/ls/{{ids}} ddf:lexicalSenseHasSemanticRelationWith dinv:{{id}}/sr/{{idr}} .

          dinv:{{id}}/sr/{{idr}} a ddf:SemanticRelation ;
            ddf:hasSemanticRelationType {{relTypeURI}} ;
            ddf:semanticRelationOfEntry {{entryURI}} .
          """
    return rdf
```

## lookUpEntries

```python
lookUpEntries(relTerms)
"""
- relTerms est une liste de string
- lance une requête SPARQL pour récupérer les URI des termes existants
- retourne une liste d'URI
"""
    listURIs = []
    for term in relTerms
        sparql = ""+loadPrefixes(dicoFR)
        sparql += """
            SELECT ?entry
            WHERE {
                ?form a ontolex:Form .
                ?form ontolex:writtenRep "{{term}}"@fr .
                ?lentry ontolex:canonicalForm ?form .
                ?entry lexicog:describes ?lentry .
                ?entry a lexicog:Entry .
            }
            """
        listURIs += query(sparql)
    return(listURIs)
```


# Préfix à utiliser

    @prefix : <http://data.dictionnairedesfrancophones.org/ontology/ddf#> .
    @prefix owl: <http://www.w3.org/2002/07/owl#> .
    @prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
    @prefix xml: <http://www.w3.org/XML/1998/namespace> .
    @prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
    @prefix core: <http://www.w3.org/2004/02/skos/core#> .
    @prefix prov: <http://www.w3.org/ns/prov-o-20130430#> .
    @prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
    @prefix sioc: <http://rdfs.org/sioc/ns#> .
    @prefix lemon: <http://lemon-model.net/lemon#> .
    @prefix lexicog: <http://www.w3.org/ns/lemon/lexicog#> .
    @prefix lexinfo: <http://www.lexinfo.net/ontology/2.0/lexinfo#> .
    @prefix ontolex: <http://www.w3.org/ns/lemon/ontolex#> .
    @prefix ddf: <http://data.dictionnairedesfrancophones.org/ontology/ddf#> .
    @prefix dinv: <http://data.dictionnairedesfrancophones.org/dict/inv/entry/> .
    @prefix dct: <http://purl.org/dc/terms/> .

    @base <http://data.dictionnairedesfrancophones.org/ontology/ddf> .

# Divers

Regex pour récupérer les prononciations de chaque entrée:
`\n\n^((\d\.\s)*[A-ZÉÈÀÇÔÎÛÊÙÏ']+[-\s,]*)+(\s*\[.*?\])*`
