# Notes Pour DicoFR

## Fichiers

- PrettyInventaire.xml : fichier xml formaté
- definition_latest.txt : dernière version du fichier txt de l'Inventaire nettoyé selon les règles définies dans `pre-traitement de l'Inventaire`
- dictionnaire_raw.txt : version brute non traitée de l'Inventaire en txt
- inventaire.xml : fichier xml non formaté
- references_raw.txt : fichier des références brutes non traitées
- references_clean.txt : fichier des références traitées
- ontolexFormSpecial.txt : fichier contenant les entrées qui ont des chiffres romains

## Inventaire

ps: Inventaire == inventaire_particularites_lexic_2850695076.pdf

### extraire les informations de l'inventaire

Pre-processing App pour extraire le contenu textuel du fichier pdf. 

- Sauvegarde du contenu textuel dans un fichier `.txt` (`dictionnaire.txt`).
- Suppression des 38 premières pages de (`dictionnaire.txt`) (de la page 1 à la page `XXXVI` du pdf).
- Création d'un fichier contenant la bibliographie `references.txt`. 


Note : Si un utilisateur veut récupérer les informations de mise en forme du pdf, il peut convertir le fichier pdf en un fichier html en utilisant `pdfbox-app-2.0.14.jar`. Nous fournissons le fichier HTML `inventaire.html`.

`java -jar pdfbox-app-2.0.14.jar ExtractText -html inventaire_particularites_lexic_2850695076.pdf inventaire.html`. 

## pre-traitement de l'Inventaire

- Suppression de certaines lignes pour faciliter certains processus automatique. 

Par exemple les numéros de pages, les entêtes
```
120



DJERMAKOYE
```

- Changement des `•`, `D`, `D 1`, `G 2'`, `D 1 '`, `Q 6', `O 2 '`, `G 1 ',`D 1 "`, `D 1 °`,   `D 2°`, ,`D l'`, `D 2°`, `• 1 "`,  `D 2'`, `D 2"`, `D i '`, `D 2"` , `G 1°`, `• 1'`, `• 1"`, `D 1 *`,  `G 1"` qui correspondent 
au petit "carré" suivi d'un chiffre par l'expression régulière  `D [0-9] '`.

- Remplacement des `//` par `Il`. 

- Ajout d'un espace supplémentaire avant ` V.` (cela correspond à Voir la définition de) pour faciliter le parsing automatique.  

- Transformation en minuscule de `V. tr`, `V. ir.`.  

## Conversion en fichier XML

Le fichier xml généré suit la structure suivante

```
<dictionnaire resource="inv">
	<lexicog:entry id="0">
		<ontolex:writtenForm></ontolex:writtenForm>
		<Lexical:Entry id="0">
			<lexicalInfo> </lexicalInfo>
			<sense>
				<ontolex:LexicalSense></ontolex:LexicalSense>
			</sense>		
	</lexicog:entry>				
</dictionnaire>
```

```
<!ELEMENT dictionnaire (lexicog:entry*)>
<!ELEMENT lexicog:entry (ontolex:writtenForm, Lexical:Entry+)>
<!ELEMENT Lexical:Entry(lexicalInfo?, sense)
<!ELEMENT sense (ontolex:LexicalSense*)>
```
