export let connectionProps = {
  "name": "lexical-entry",
  "fields": [
    {
      "fieldName": "canonicalForm",
      "propertyChain": [
        "http://www.w3.org/ns/lemon/ontolex#canonicalForm",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "senses",
      "propertyChain": [
        "http://www.w3.org/ns/lemon/ontolex#sense",
      ],
      "analyzed": false,
      "multivalued": true
    },
    {
      "fieldName": "entry",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#isDescribedBy",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "grammaticalProperties",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasGrammaticalProperty",
      ],
      "analyzed": false,
      "multivalued": true
    },
    {
      "fieldName": "partOfSpeech",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasPartOfSpeech",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "transitivity",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasTransitivity",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "mood",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasMood",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "tense",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasTense",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "gender",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasGender",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "number",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasNumber",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "termElement",
      "propertyChain": [
        "http://www.lexinfo.net/ontology/2.0/lexinfo#termElement",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "multiWordType",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#multiWordType",
      ],
      "analyzed": false,
      "multivalued": false
    },
  ],
  "types": [
    "http://www.w3.org/ns/lemon/ontolex#LexicalEntry"
  ],
  "elasticsearchNode": "http://docker.for.mac.localhost:9200",
};
