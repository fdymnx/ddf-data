export let connectionProps = {
  "name": "lexical-sense",
  "fields": [
    {
      "fieldName": "definition",
      "propertyChain": [
        "http://www.w3.org/2004/02/skos/core#definition"
      ],
      "analyzed": true,
      "multivalued": false
    },
    {
      "fieldName": "isSenseOf",
      "propertyChain": [
        "http://www.w3.org/ns/lemon/ontolex#isSenseOf",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "hasLocalisation",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasLocalisation"
      ],
      "analyzed": false,
      "multivalued": true
    },
    {
      "fieldName": "usageExample",
      "propertyChain": [
        "http://www.w3.org/ns/lemon/lexicog#usageExample"
      ],
      "analyzed": false,
      "multivalued": true
    },
    {
      "fieldName": "hasConnotation",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasConnotation"
      ],
      "analyzed": true,
      "multivalued": true,
    },
    {
      "fieldName": "hasDomain",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasDomain"
      ],
      "analyzed": true,
      "multivalued": true
    },
    {
      "fieldName": "hasFrequency",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasFrequency"
      ],
      "analyzed": true,
      "multivalued": true,
    },
    {
      "fieldName": "hasGlossary",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasGlossary"
      ],
      "analyzed": true,
      "multivalued": true
    },
    {
      "fieldName": "hasGrammaticalConstraint",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasGrammaticalConstraint"
      ],
      "analyzed": true,
      "multivalued": true,
    },
    {
      "fieldName": "hasRegister",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasRegister"
      ],
      "analyzed": true,
      "multivalued": true,
    },
    {
      "fieldName": "hasSociolect",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasSociolect"
      ],
      "analyzed": true,
      "multivalued": true,
    },
    {
      "fieldName": "hasTemporality",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasTemporality"
      ],
      "analyzed": true,
      "multivalued": true,
    },
    {
      "fieldName": "hasTextualGenre",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasTextualGenre"
      ],
      "analyzed": true,
      "multivalued": true,
    },
    {
      "fieldName": "places",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasLocalisation"
      ],
      "analyzed": false,
      "multivalued": true,
    },
    {
      "fieldName": "geoloc",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasLocalisation",
        "http://www.opengis.net/ont/geosparql#hasGeometry",
        "http://www.opengis.net/ont/geosparql#asGML"
      ],
      "analyzed": true,
      "multivalued": true,
      "datatype": "native:geo_point"
    },
    {
      "fieldName": "processed",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#processed",
      ],
      "analyzed": true,
      "multivalued": false,
      "datatype": "xsd:boolean",
      "defaultValue": '\\\"false\\\"^^xsd:boolean'
    },
    {
      "fieldName": "hasValidationRating",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasAggregationRating",
      ],
      "analyzed": false,
      "multivalued": true
    },
    {
      "fieldName": "hasSuppressionRating",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasAggregationRating",
      ],
      "analyzed": false,
      "multivalued": true
    },{
      "fieldName": "hasReportingRating",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasAggregationRating",
      ],
      "analyzed": false,
      "multivalued": true
    }
  ],
  "types": [
    "http://www.w3.org/ns/lemon/ontolex#LexicalSense"
  ],
  "entityFilter": "(?hasValidationRating -> rdf:type in (<http://data.dictionnairedesfrancophones.org/ontology/ddf#ValidationRating>)) && (?hasSuppressionRating -> rdf:type in (<http://data.dictionnairedesfrancophones.org/ontology/ddf#SuppressionRating>)) && (?hasReportingRating -> rdf:type in (<http://data.dictionnairedesfrancophones.org/ontology/ddf#ReportingRating>))"
};

