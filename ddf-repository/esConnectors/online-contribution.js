export let connectionProps = {
  "name": "online-contribution",
  "fields": [
    {
      "fieldName": "content",
      "propertyChain": [
        "http://rdfs.org/sioc/ns#content",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "sense",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#aboutSense",
      ],
      "analyzed": false,
      "multivalued": false
    },
     {
      "fieldName": "lexicalEntry",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#aboutEtymology",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "form",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#aboutForm",
      ],
      "analyzed": false,
      "multivalued": false
    }
  ],
  "types": [
    "http://rdfs.org/sioc/ns#Item"
  ],
   "elasticsearchNode": "http://docker.for.mac.localhost:9200",
};

