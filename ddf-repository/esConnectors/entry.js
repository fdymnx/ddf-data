export let connectionProps = {
  "name": "entry",
  "fields": [
    {
      "fieldName": "lexicalEntry",
      "propertyChain": [
        "http://www.w3.org/ns/lemon/lexicog#describes",
      ],
      "analyzed": false,
      "multivalued": true
    },
    {
      "fieldName": "lexicographicResource",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasLexicographicResource",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "semanticRelations",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#entryHasSemanticRelationWith",
      ],
      "analyzed": false,
      "multivalued": true
    },
    {
      "fieldName": "actions",
      "propertyChain": [
        "http://www.w3.org/ns/prov#generated",
        "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
      ],
      "analyzed": false,
      "multivalued": true
    }
  ],
  "types": [
    "http://www.w3.org/ns/lemon/lexicog#Entry"
  ],
  "elasticsearchNode": "http://docker.for.mac.localhost:9200",
};

