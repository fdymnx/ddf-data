export let connectionProps = {
  "name": "form",
  "fields": [
    {
      "fieldName": "writtenRep",
      "propertyChain": [
        "http://www.w3.org/ns/lemon/ontolex#writtenRep",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "lexicalEntries",
      "propertyChain": [
        "http://ns.mnemotix.com/ontologies/2019/1/indexing-model#isCanonicalFormOf",
      ],
      "analyzed": false,
      "multivalued": true
    },
    {
      "fieldName": "places",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#formHasLocalisation"
      ],
      "analyzed": false,
      "multivalued": true,
    },
    {
      "fieldName": "geoloc",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#formHasLocalisation",
        "http://www.opengis.net/ont/geosparql#hasGeometry",
        "http://www.opengis.net/ont/geosparql#asWKT"
      ],
      "analyzed": false,
      "multivalued": true,
      "datatype": "native:geo_point"
    },
  ],
  "types": [
    "http://www.w3.org/ns/lemon/ontolex#Form"
  ],
   "elasticsearchNode": "http://docker.for.mac.localhost:9200",
};

