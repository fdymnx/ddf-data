export let connectionProps = {
  "name": "voc-place",
  "fields": [
    {
      "fieldName": "prefLabel",
      "propertyChain": [
        "http://www.w3.org/2004/02/skos/core#prefLabel",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "altLabel",
      "propertyChain": [
        "http://www.w3.org/2004/02/skos/core#altLabel",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "inScheme",
      "propertyChain": [
        "http://www.w3.org/2004/02/skos/core#inScheme",
      ],
      "analyzed": false,
      "multivalued": false
    },{
      "fieldName": "color",
      "propertyChain": [
        "http://ns.mnemotix.com/ontologies/2019/8/generic-model/color",
      ],
      "analyzed": false,
      "multivalued": false
    }
  ],
  "types": [
    "http://www.w3.org/2004/02/skos/core#Concept"
  ],
  "entityFilter":"bound(?inScheme) && ?inScheme in (<http://data.dictionnairedesfrancophones.org/authority/place>)",
  "elasticsearchNode": "http://docker.for.mac.localhost:9200",
};

