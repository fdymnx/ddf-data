export let connectionProps = {
  "name": "action",
  "fields": [
    {
      "fieldName": "startedAtTime",
      "propertyChain": [
        "http://www.w3.org/ns/prov#startedAtTime",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "hasUserAccount",
      "propertyChain": [
        "http://www.w3.org/ns/prov#wasAttributedTo",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "hasEntity",
      "propertyChain": [
        "http://www.w3.org/ns/prov#generated",
      ],
      "analyzed": false,
      "multivalued": true
    },
  ],
  "types": [
    "http://ns.mnemotix.com/ontologies/2019/8/generic-model/Action"
  ],
  "elasticsearchNode": "http://docker.for.mac.localhost:9200",
};

