export let connectionProps = {
  "name": "semantic-relation",
  "fields": [
    {
      "fieldName": "entries",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#semanticRelationOfEntry",
      ],
      "analyzed": false,
      "multivalued": true
    },
    {
      "fieldName": "lexicalSenses",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#semanticRelationOfLexicalSense",
      ],
      "analyzed": false,
      "multivalued": true
    },
    {
      "fieldName": "lexicalEntries",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#semanticRelationOfLexicalEntry",
      ],
      "analyzed": false,
      "multivalued": true
    },
    {
      "fieldName": "semanticRelationType",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#semanticRelationType",
      ],
      "analyzed": false,
      "multivalued": false
    },
  ],
  "types": [
    "http://data.dictionnairedesfrancophones.org/ontology/ddf#SemanticRelation"
  ],
  "elasticsearchNode": "http://docker.for.mac.localhost:9200",
};

