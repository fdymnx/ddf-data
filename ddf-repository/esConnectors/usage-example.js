export let connectionProps = {
  "name": "usage-example",
  "fields": [
    {
      "fieldName": "value",
      "propertyChain": [
        "http://www.w3.org/1999/02/22-rdf-syntax-ns#value"
      ],
      "analyzed": false,
      "multivalued": true
    },
    {
      "fieldName": "bibliographicalCitation",
      "propertyChain": [
        "http://purl.org/dc/terms/bibliographicalCitation"
      ],
      "analyzed": false,
      "multivalued": true
    },
    {
      "fieldName": "lexicalSense",
      "propertyChain": [
        "http://ns.mnemotix.com/ontologies/2019/1/indexing-model#isUsageExampleOf"
      ],
      "analyzed": false,
      "multivalued": true
    },
  ],
  "types": [
    "http://www.w3.org/ns/lemon/lexicog#UsageExample"
  ],
  "elasticsearchNode": "http://docker.for.mac.localhost:9200",
};

