import {connectionProps as entryConnectionProps} from "./entry";
import {connectionProps as formConnectionProps} from "./form";
import {connectionProps as lexicalEntryConnectionProps} from "./lexical-entry";
import {connectionProps as lexicalSenseConnectionProps} from "./lexical-sense";
import {connectionProps as onlineContributionConnectionProps} from "./online-contribution";
import {connectionProps as semanticRelationConnectionProps} from "./semantic-relation";
import {connectionProps as usageExampleConnectionProps} from "./usage-example";
import {connectionProps as vocGrammaticalPropertyConnectionProps} from "./voc-grammatical-property";
import {connectionProps as vocLexicographicResourceConnectionProps} from "./voc-lexicographic-resource";
import {connectionProps as vocPlaceConnectionProps} from "./voc-place";
import {connectionProps as vocSemanticPropertyConnectionProps} from "./voc-semantic-property";
import {connectionProps as personConnectionProps} from "./person";
import {connectionProps as userAccountConnectionProps} from "./user-account";
import {connectionProps as userGroupConnectionProps} from "./user-group";
import {connectionProps as actionConnectionProps} from "./action";
import {connectionProps as aggregateRatingConnectionProps} from "./aggregate-rating";


export {commonFields, commonEntityFilter} from "./_commonFields";
export const connectors = {
  // personConnectionProps,
  // userAccountConnectionProps,
  // actionConnectionProps,
  // userGroupConnectionProps,
  // entryConnectionProps,
   formConnectionProps,
  // lexicalEntryConnectionProps,
  // onlineContributionConnectionProps,
  // semanticRelationConnectionProps,
  // usageExampleConnectionProps,
  // vocPlaceConnectionProps,
  // vocLexicographicResourceConnectionProps,
  // vocSemanticPropertyConnectionProps,
  // vocGrammaticalPropertyConnectionProps,
  //aggregateRatingConnectionProps
};
