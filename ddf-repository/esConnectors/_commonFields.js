export let commonFields = [
  {
    "fieldName": "types",
    "propertyChain": [
      "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
    ],
    "analyzed": false,
    "multivalued": true
  },
  {
    "fieldName": "createdAt",
    "propertyChain": [
      "http://www.w3.org/ns/prov#generated",
      "http://www.w3.org/ns/prov#startedAtTime"
    ],
    "analyzed": true,
    "multivalued": false,
    "datatype": "xsd:dateTime"
  },
  {
    "fieldName": "hasCreationAction",
    "propertyChain": [
      "http://www.w3.org/ns/prov#generated",
    ],
    "analyzed": false,
    "multivalued": false
  }, {
    "fieldName": "hasDeletionAction",
    "propertyChain": [
      "http://www.w3.org/ns/prov#generated",
    ],
    "analyzed": false,
    "multivalued": true
  }
];

export let commonEntityFilter = "!bound(?hasDeletionAction) && (?hasDeletionAction -> rdf:type in (<http://ns.mnemotix.com/ontologies/2019/8/generic-model/Deletion>)) && (?hasCreationAction -> rdf:type in (<http://ns.mnemotix.com/ontologies/2019/8/generic-model/Creation>))";
