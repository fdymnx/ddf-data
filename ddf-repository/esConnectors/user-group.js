export let connectionProps = {
  "name": "user-group",
  "fields": [
    {
      "fieldName": "label",
      "propertyChain": [
        "http://www.w3.org/2000/01/rdf-schema#label",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "hasUserAccount",
      "propertyChain": [
        "http://rdfs.org/sioc/ns#has_member",
      ],
      "analyzed": false,
      "multivalued": true
    }
  ],
  "types": [
    "http://ns.mnemotix.com/ontologies/2019/8/generic-model/UserGroup"
  ],
  "elasticsearchNode": "http://docker.for.mac.localhost:9200",
};

