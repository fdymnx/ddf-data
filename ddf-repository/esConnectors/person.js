export let connectionProps = {
  "name": "person",
  "fields": [
    {
      "fieldName": "nickName",
      "propertyChain": [
        "http://xmlns.com/foaf/0.1/nick",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "linguisticProfile",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#linguisticProfile",
      ],
      "analyzed": true,
      "multivalued": false
    },
    {
      "fieldName": "otherInformation",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#otherInformation",
      ],
      "analyzed": true,
      "multivalued": false
    },
    {
      "fieldName": "otherSpokenLanguages",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#otherSpokenLanguages",
      ],
      "analyzed": true,
      "multivalued": false
    },
    {
      "fieldName": "gender",
      "propertyChain": [
        "http://xmlns.com/foaf/0.1/gender",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "yearOfBirth",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#yearOfBirth",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "userAccount",
      "propertyChain": [
        "http://xmlns.com/foaf/0.1/account",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "emailAccounts",
      "propertyChain": [
        "http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasEmailAccount",
      ],
      "analyzed": false,
      "multivalued": true
    },
    {
      "fieldName": "place",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#hasLocalisation",
      ],
      "analyzed": false,
      "multivalued": true
    }, {
      "fieldName": "preferredDomain",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#preferredDomain",
      ],
      "analyzed": false,
      "multivalued": true
    },

  ],
  "types": [
    "http://data.dictionnairedesfrancophones.org/ontology/ddf#Person"
  ],
  "entityFilter": "(?userAccount -> rdf:type not in (<http://ns.mnemotix.com/ontologies/2019/8/generic-model/EmailAccount>)) && (?emailAccounts -> rdf:type not in (<http://ns.mnemotix.com/ontologies/2019/8/generic-model/UserAccount>))",
  "elasticsearchNode": "http://docker.for.mac.localhost:9200",
};

