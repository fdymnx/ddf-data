export let connectionProps = {
  "name": "user-account",
  "fields": [
    {
      "fieldName": "userId",
      "propertyChain": [
        "http://ns.mnemotix.com/ontologies/2019/8/generic-model/userId",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "username",
      "propertyChain": [
        "http://ns.mnemotix.com/ontologies/2019/8/generic-model/username",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "isUserDisabled",
      "propertyChain": [
        "http://ns.mnemotix.com/ontologies/2019/8/generic-model/isUserDisabled",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "isUserUnregistered",
      "propertyChain": [
        "http://ns.mnemotix.com/ontologies/2019/8/generic-model/isUserUnregistered",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "hasUserGroup",
      "propertyChain": [
        "http://rdfs.org/sioc/ns#member_of",
      ],
      "analyzed": false,
      "multivalued": true
    }
  ],
  "types": [
    "http://ns.mnemotix.com/ontologies/2019/8/generic-model/UserAccount"
  ],
  "elasticsearchNode": "http://docker.for.mac.localhost:9200",
};

