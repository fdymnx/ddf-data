export let connectionProps = {
  "name": "aggregate-rating",
  "fields": [
    {
      "fieldName": "hasRater",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#rater",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "hasEntity",
      "propertyChain": [
        "https://schema.org/itemReviewed",
      ],
      "analyzed": false,
      "multivalued": true
    },
  ],
  "types": [
    "https://schema.org/AggregateRating"
  ],
  "elasticsearchNode": "http://docker.for.mac.localhost:9200",
};
