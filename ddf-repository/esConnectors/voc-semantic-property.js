export let connectionProps = {
  "name": "voc-semantic-property",
  "fields": [
    {
      "fieldName": "prefLabel",
      "propertyChain": [
        "http://www.w3.org/2004/02/skos/core#prefLabel",
      ],
      "analyzed": false,
      "multivalued": false
    },{
      "fieldName": "definition",
      "propertyChain": [
        "http://www.w3.org/2004/02/skos/core#definition",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "inScheme",
      "propertyChain": [
        "http://www.w3.org/2004/02/skos/core#inScheme",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "topConceptOf",
      "propertyChain": [
        "http://www.w3.org/2004/02/skos/core#topConceptOf",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "broader",
      "propertyChain": [
        "http://www.w3.org/2004/02/skos/core#broader",
      ],
      "analyzed": false,
      "multivalued": true
    },
    {
      "fieldName": "narrower",
      "propertyChain": [
        "http://www.w3.org/2004/02/skos/core#narrower",
      ],
      "analyzed": false,
      "multivalued": true
    },
    {
      "fieldName": "related",
      "propertyChain": [
        "http://www.w3.org/2004/02/skos/core#related",
      ],
      "analyzed": false,
      "multivalued": true
    },
    {
      "fieldName": "lexicalSenses",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#isSemanticPropertyOf",
      ],
      "analyzed": false,
      "multivalued": true
    }
  ],
  "types": [
    "http://www.w3.org/2004/02/skos/core#Concept"
  ],
  "entityFilter":"bound(?inScheme) && ?inScheme in (<http://data.dictionnairedesfrancophones.org/authority/textual-genre>,  <http://data.dictionnairedesfrancophones.org/authority/sociolect>, <http://data.dictionnairedesfrancophones.org/authority/temporality>,  <http://data.dictionnairedesfrancophones.org/authority/grammatical-constraint>, <http://data.dictionnairedesfrancophones.org/authority/register>, <http://data.dictionnairedesfrancophones.org/authority/glossary>, <http://data.dictionnairedesfrancophones.org/authority/frequency>, <http://data.dictionnairedesfrancophones.org/authority/connotation>, <http://data.dictionnairedesfrancophones.org/authority/sense-relation>, <http://data.dictionnairedesfrancophones.org/authority/domain>, <http://data.dictionnairedesfrancophones.org/authority/form-type>)",
  "elasticsearchNode": "http://docker.for.mac.localhost:9200",
};

