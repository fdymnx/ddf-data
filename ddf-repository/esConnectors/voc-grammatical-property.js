export let connectionProps = {
  "name": "voc-grammatical-property",
  "fields": [
    {
      "fieldName": "prefLabel",
      "propertyChain": [
        "http://www.w3.org/2004/02/skos/core#prefLabel",
      ],
      "analyzed": false,
      "multivalued": false
    },{
      "fieldName": "definition",
      "propertyChain": [
        "http://www.w3.org/2004/02/skos/core#definition",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "inScheme",
      "propertyChain": [
        "http://www.w3.org/2004/02/skos/core#inScheme",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "topConceptOf",
      "propertyChain": [
        "http://www.w3.org/2004/02/skos/core#topConceptOf",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "broader",
      "propertyChain": [
        "http://www.w3.org/2004/02/skos/core#broader",
      ],
      "analyzed": false,
      "multivalued": true
    },
    {
      "fieldName": "narrower",
      "propertyChain": [
        "http://www.w3.org/2004/02/skos/core#narrower",
      ],
      "analyzed": false,
      "multivalued": true
    },
    {
      "fieldName": "related",
      "propertyChain": [
        "http://www.w3.org/2004/02/skos/core#related",
      ],
      "analyzed": false,
      "multivalued": true
    },
    {
      "fieldName": "lexicalEntries",
      "propertyChain": [
        "http://data.dictionnairedesfrancophones.org/ontology/ddf#isGrammaticalPropertyOf",
      ],
      "analyzed": false,
      "multivalued": true
    },
  ],
  "types": [
    "http://www.w3.org/2004/02/skos/core#Concept"
  ],
  "entityFilter":"bound(?inScheme) && ?inScheme in (<http://data.dictionnairedesfrancophones.org/authority/verb-frame>, <http://data.dictionnairedesfrancophones.org/authority/gender>, <http://data.dictionnairedesfrancophones.org/authority/mood>, <http://data.dictionnairedesfrancophones.org/authority/mood>, <http://data.dictionnairedesfrancophones.org/authority/multiword-type>, <http://data.dictionnairedesfrancophones.org/authority/number>, <http://data.dictionnairedesfrancophones.org/authority/part-of-speech-type>, <http://data.dictionnairedesfrancophones.org/authority/tense>, <http://data.dictionnairedesfrancophones.org/authority/term-element>, <http://data.dictionnairedesfrancophones.org/authority/transitivity>)",
  "elasticsearchNode": "http://docker.for.mac.localhost:9200",
};

