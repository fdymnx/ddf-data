export let connectionProps = {
  "name": "voc-lexicographic-resource",
  "fields": [
    {
      "fieldName": "prefLabel",
      "propertyChain": [
        "http://www.w3.org/2004/02/skos/core#prefLabel",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "altLabel",
      "propertyChain": [
        "http://www.w3.org/2004/02/skos/core#altLabel",
      ],
      "analyzed": false,
      "multivalued": false
    },
    {
      "fieldName": "inScheme",
      "propertyChain": [
        "http://www.w3.org/2004/02/skos/core#inScheme",
      ],
      "analyzed": false,
      "multivalued": false
    }
  ],
  "types": [
    "http://www.w3.org/2004/02/skos/core#Concept"
  ],
  "entityFilter": "bound(?inScheme) && ?inScheme in (<http://data.dictionnairedesfrancophones.org/authority/lexicographical-resource>)",
  "elasticsearchNode": "http://docker.for.mac.localhost:9200",
};

