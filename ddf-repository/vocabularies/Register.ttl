@prefix dct: <http://purl.org/dc/terms/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

<http://data.dictionnairedesfrancophones.org/authority/register> a skos:ConceptScheme;
  dct:created "2019-02-25T00:00:00.000Z"^^xsd:dateTime;
  dct:creator "Noé Gasparini, Antoine Bouchez"@fr;
  dct:description "manière de s’exprimer, en fonction de l’interlocuteur. Si l’interlocuteur est une personne proche, le registre sera plutôt familier alors que si l’interlocuteur est un supérieur hiérarchique, il sera plutôt soutenu."@fr,
    "way to talk depending on the conversation partner. If the partner is a close friend, the register will tend to be informal whereas it would rather be formal with a superior"@en;
  dct:isRequiredBy <http://data.dictionnairedesfrancophones.org/ontology/ddf>;
  dct:license <http://creativecommons.org/licenses/by/4.0/>;
  dct:modified "2019-04-29T00:00:00.000Z"^^xsd:dateTime;
  dct:publisher "Ddf"@fr;
  dct:rights "cc by 4.0"@fr;
  dct:title "Register"@en, "Registre"@fr;
  dct:type "Vocabulaire contrôlé"@fr;
  skos:hasTopConcept <http://data.dictionnairedesfrancophones.org/authority/register/childishRegister>,
    <http://data.dictionnairedesfrancophones.org/authority/register/informalRegister>,
    <http://data.dictionnairedesfrancophones.org/authority/register/traditionalRegister>,
    <http://data.dictionnairedesfrancophones.org/authority/register/veryInformalRegister>,
    <http://www.lexinfo.net/ontology/2.0/lexinfo#formalRegister>, <http://www.lexinfo.net/ontology/2.0/lexinfo#vulgarRegister>;
  skos:prefLabel "Registre"@fr .

<http://data.dictionnairedesfrancophones.org/authority/register/informalRegister>
  a skos:Concept;
  skos:definition "utilisé entre personnes proches"@fr;
  skos:editorialNote "incluant oral"@fr;
  skos:inScheme <http://data.dictionnairedesfrancophones.org/authority/register>;
  skos:notation "dial."^^<http://data.dictionnairedesfrancophones.org/resource/inventaire>,
    "fam."^^<http://data.dictionnairedesfrancophones.org/resource/inventaire>, "oral."^^<http://data.dictionnairedesfrancophones.org/resource/inventaire>,
    "{{familier|fr}}"^^<http://data.dictionnairedesfrancophones.org/resource/wiktionnaire>,
    "{{informel|fr}}"^^<http://data.dictionnairedesfrancophones.org/resource/wiktionnaire>;
  skos:prefLabel "familier"@fr;
  skos:topConceptOf <http://data.dictionnairedesfrancophones.org/authority/register> .

<http://data.dictionnairedesfrancophones.org/authority/register/veryInformalRegister>
  a skos:Concept;
  skos:definition "utilisé entre personnes très proches"@fr;
  skos:inScheme <http://data.dictionnairedesfrancophones.org/authority/register>;
  skos:notation "pop."^^<http://data.dictionnairedesfrancophones.org/resource/inventaire>,
    "{{populaire|fr}}"^^<http://data.dictionnairedesfrancophones.org/resource/wiktionnaire>,
    "{{très familier|fr}}"^^<http://data.dictionnairedesfrancophones.org/resource/wiktionnaire>;
  skos:prefLabel "très familier"@fr;
  skos:topConceptOf <http://data.dictionnairedesfrancophones.org/authority/register> .

<http://www.lexinfo.net/ontology/2.0/lexinfo#vulgarRegister> a skos:Concept;
  skos:definition "langage impoli, injurieux"@fr;
  skos:inScheme <http://data.dictionnairedesfrancophones.org/authority/register>;
  skos:notation "vulg."^^<http://data.dictionnairedesfrancophones.org/resource/inventaire>,
    "{{vulgaire|fr}}"^^<http://data.dictionnairedesfrancophones.org/resource/wiktionnaire>,
    "{{vulg|fr}}"^^<http://data.dictionnairedesfrancophones.org/resource/wiktionnaire>;
  skos:prefLabel "vulgaire"@fr;
  skos:topConceptOf <http://data.dictionnairedesfrancophones.org/authority/register> .

<http://www.lexinfo.net/ontology/2.0/lexinfo#formalRegister> a skos:Concept;
  skos:definition "mots rares et savants"@fr;
  skos:inScheme <http://data.dictionnairedesfrancophones.org/authority/register>;
  skos:notation "{{formel|fr}}"^^<http://data.dictionnairedesfrancophones.org/resource/wiktionnaire>,
    "{{soutenu|fr}}"^^<http://data.dictionnairedesfrancophones.org/resource/wiktionnaire>,
    "{{sout|fr}}"^^<http://data.dictionnairedesfrancophones.org/resource/wiktionnaire>,
    "écrit."^^<http://data.dictionnairedesfrancophones.org/resource/inventaire>;
  skos:prefLabel "soutenu"@fr;
  skos:topConceptOf <http://data.dictionnairedesfrancophones.org/authority/register> .

<http://data.dictionnairedesfrancophones.org/authority/register/traditionalRegister>
  a skos:Concept;
  skos:definition "ancré dans la tradition culturelle"@fr;
  skos:inScheme <http://data.dictionnairedesfrancophones.org/authority/register>;
  skos:notation "trad."^^<http://data.dictionnairedesfrancophones.org/resource/inventaire>;
  skos:prefLabel "traditionnel"@fr;
  skos:topConceptOf <http://data.dictionnairedesfrancophones.org/authority/register> .

<http://data.dictionnairedesfrancophones.org/authority/register/childishRegister>
  a skos:Concept;
  skos:definition "utilisé par ou avec des enfants"@fr;
  skos:inScheme <http://data.dictionnairedesfrancophones.org/authority/register>;
  skos:notation "{{enfantin|fr}}"^^<http://data.dictionnairedesfrancophones.org/resource/wiktionnaire>,
    "{{enfant|fr}}"^^<http://data.dictionnairedesfrancophones.org/resource/wiktionnaire>;
  skos:prefLabel "langage enfantin"@fr;
  skos:topConceptOf <http://data.dictionnairedesfrancophones.org/authority/register> .
