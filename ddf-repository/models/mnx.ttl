@prefix : <http://ns.mnemotix.com/ontologies/2019/8/generic-model/> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix xml: <http://www.w3.org/XML/1998/namespace> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix prov: <http://www.w3.org/ns/prov#> .
@prefix s4ac: <http://ns.inria.fr/s4ac/v2/> .
@prefix dc: <http://purl.org/dc/elements/1.1/> .
@prefix dct: <http://purl.org/dc/terms/> .
@base <http://ns.mnemotix.com/ontologies/2019/8/generic-model/> .

<http://ns.mnemotix.com/ontologies/2019/8/generic-model> rdf:type owl:Ontology ;
    owl:imports <http://www.w3.org/2001/XMLSchema#>, <http://www.w3.org/1999/02/22-rdf-syntax-ns#>, <http://www.w3.org/2000/01/rdf-schema#>, <http://www.w3.org/ns/prov#> ;
    dc:creator "Mnemotix SCIC"@fr ;
    dc:created "2019-08-01"^^xsd:dateTimestamp;
    dct:modified "2019-10-29"^^xsd:dateTimestamp;
    dct:license "";
    dc:description "Mnémotix generic and cross-domain ontology gathering primary classes used in most of our projects"@en ;
    dc:title "Mnémotix Generic Model"@en , "Ontologie générique Mnémotix"@fr ;
    owl:versionInfo "1.1" ;
    owl:priorVersion "1.0" ;
    foaf:depiction <https://gitlab.com/mnemotix/mnx-models/raw/b92a8017561915dbb8be1b44507af514d7fd2092/ontologie-mnx.jpg?inline=false> ;
    dc:abstract "Our abstract.".



############################################################
#==============   mnx-common  ================================#
############################################################

#################################################################
#    Classes
#################################################################

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/Entity
:Entity rdf:type owl:Class;
  rdfs:subClassOf prov:Entity ; 
  rdfs:label "Entity"@en .


#################################################################
#    Object Properties
#################################################################

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasExternalLink
:hasExternalLink rdf:type owl:ObjectProperty ;
  rdfs:domain :Entity ;
  rdfs:range :ExternalLink .

## see https://www.w3.org/TR/prov-o/#wasGeneratedBy
### prov:wasGeneratedBy
prov:wasGeneratedBy rdf:type owl:ObjectProperty ;
  owl:inverseOf prov:generated ;
  rdfs:domain prov:Entity ;
  rdfs:range  prov:Activity .

## see https://www.w3.org/TR/prov-o/#generated 
### prov:generated
prov:generated rdf:type owl:ObjectProperty ;
  owl:inverseOf prov:wasGeneratedBy ;
  rdfs:domain prov:Activity ;
  rdfs:range  prov:Entity .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasAccessPolicy
:hasAccessPolicy rdf:type owl:ObjectProperty ;
  rdfs:domain :Entity ;
  rdfs:range :AccessPolicy .

############################################################
#==============   mnx-acl  ================================#
############################################################

#################################################################
#    Classes
#################################################################

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/AccessPolicy
:AccessPolicy rdf:type owl:Class;
  rdfs:label "AccessPolicy"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/AccessPolicyTarget
:AccessPolicyTarget rdf:type owl:Class;
  rdfs:label "AccessPolicyTarget"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/AccessScope
:AccessScope rdf:type owl:Class;
  rdfs:label "AccessScope"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/GroupScope
:GroupScope rdf:type owl:Class;
  rdfs:subClassOf :AccessScope ;
  rdfs:label "GroupScope"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/GlobalScope
:GlobalScope rdf:type owl:Class;
  rdfs:subClassOf :AccessScope ;
  rdfs:label "GlobalScope"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/IndividualScope
:IndividualScope rdf:type owl:Class;
  rdfs:subClassOf :AccessScope ;
  rdfs:label "IndividualScope"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/AccessPrivilege
:AccessPrivilege rdf:type owl:Class;
  rdfs:label "AccessPrivilege"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/ReadPrivilege
:ReadPrivilege rdf:type owl:Class;
  rdfs:subClassOf :AccessPrivilege ;
  rdfs:label "ReadPrivilege"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/WritePrivilege
:WritePrivilege rdf:type owl:Class;
  rdfs:subClassOf :AccessPrivilege ;
  rdfs:label "WritePrivilege"@en .


#################################################################
#    Object Properties
#################################################################

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasAccessPrivilege
:hasAccessPrivilege rdf:type owl:ObjectProperty ;
  rdfs:domain :AccessPolicy ;
  rdfs:range :AccessPrivilege .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasScope
:hasScope rdf:type owl:ObjectProperty ;
  rdfs:domain :AccessPolicy ;
  rdfs:range :AccessScope .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasAccessPolicy
:hasAccessPolicy rdf:type owl:ObjectProperty ;
  rdfs:domain <http://www.w3.org/2001/XMLSchema#anyURI> ;
  rdfs:range :AccessPolicy . 

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasPolicyTarget
:hasPolicyTarget rdf:type owl:ObjectProperty ;
  rdfs:domain <http://www.w3.org/2001/XMLSchema#anyURI> ;
  rdfs:range :AccessPolicyTarget .

#################################################################
#    Instances 
#################################################################

# Access policy génériques 
:GroupPolicy a :AccessPolicy ;
    :hasScope  :GroupScope ; 
	:hasAccessPrivilege	:WritePrivilege ;
	:hasAccessPrivilege	:ReadPrivilege .
		
:GroupReadPolicy a :AccessPolicy ;
    :hasScope :GroupScope ;
    :hasAccessPrivilege	:ReadPrivilege .
		
:GlobalPolicy a :AccessPolicy ;
    :hasScope :GlobalScope ;
    :hasAccessPrivilege	:ReadPrivilege .		
		
:IndividualPolicy a :AccessPolicy ;
    :hasScope :GlobalScope ;
	:hasAccessPrivilege	:WritePrivilege ;
    :hasAccessPrivilege	:ReadPrivilege .


############################################################
#==============   mnx-agent  ================================#
############################################################
#################################################################
#    Classes
#################################################################

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/Address
:Address rdf:type owl:Class;
  rdfs:subClassOf :Location, :SpatialEntity, :MaterialEntity, :Entity ;
  rdfs:label "Address"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/Affiliation
:Affiliation rdf:type owl:Class;
  rdfs:subClassOf :Duration, :TemporalEntity, :MaterialEntity, :Entity ;
  rdfs:label "Affiliation"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/Agent
:Agent rdf:type owl:Class;
  rdfs:subClassOf foaf:Agent ;
  rdfs:subClassOf :Entity ;
  rdfs:label "Agent"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/EmailAccount
:EmailAccount rdf:type owl:Class;
  rdfs:subClassOf foaf:OnlineAccount ; 
  rdfs:subClassOf :Entity ;
  rdfs:label "EmailAccount"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/Group
:Group rdf:type owl:Class;
  rdfs:subClassOf :Entity ;
  rdfs:label "Group"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/GroupMembership
:GroupMembership rdf:type owl:Class;
  rdfs:subClassOf :Entity ;
  rdfs:label "GroupMembership"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/Organization
:Organization rdf:type owl:Class;
  rdfs:subClassOf foaf:Organization ; 
  rdfs:subClassOf :Agent, :Entity ;
  rdfs:label "Organization"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/Partnership
:Partnership rdf:type owl:Class;
  rdfs:subClassOf :Entity ;
  rdfs:label "Partnership"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/Person
:Person rdf:type owl:Class;
  rdfs:subClassOf foaf:Person ; 
  rdfs:subClassOf :Agent, :Entity ;
  rdfs:label "Person"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/Phone
:Phone rdf:type owl:Class;
  rdfs:subClassOf :Entity ;
  rdfs:label "Phone"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/UserAccount
:UserAccount rdf:type owl:Class;
  rdfs:subClassOf sioc:User ; 
  rdfs:subClassOf :AccessPolicyTarget, :Entity ;
  rdfs:label "UserAccount"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/UserGroup
:UserGroup rdf:type owl:Class;
  rdfs:subClassOf sioc:UserGroup ; 
  rdfs:subClassOf :AccessPolicyTarget, :Entity ;
  rdfs:label "UserGroup"@en .


#################################################################
#    Object Properties
#################################################################

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasAddress
:hasAddress rdf:type owl:ObjectProperty ;
  owl:maxCardinality "1"^^xsd:int ;
  rdfs:domain :Agent ;
  rdfs:range :Address .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasPerson
:hasPerson rdf:type owl:ObjectProperty ;
  owl:maxCardinality "1"^^xsd:int ;
  rdfs:domain :Affiliation ;
  rdfs:range :Person .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasOrg
:hasOrg rdf:type owl:ObjectProperty ;
  owl:maxCardinality "1"^^xsd:int ;
  rdfs:domain :Affiliation ;
  rdfs:range :Organization .

### foaf:account
foaf:account rdf:type owl:ObjectProperty ;
  rdfs:domain :Agent ;
  rdfs:range :EmailAccount .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasPhone
:hasPhone rdf:type owl:ObjectProperty ;
  rdfs:domain :Agent ;
  rdfs:range :Phone .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasAddress
:hasAddress rdf:type owl:ObjectProperty ;
  rdfs:domain :Agent ;
  rdfs:range :Address .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasAgent
:hasAgent rdf:type owl:ObjectProperty ;
  rdfs:domain :Involvement ;
  rdfs:range :Agent .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasEmailAccount
:hasEmailAccount rdf:type owl:ObjectProperty ;
  owl:maxCardinality "1"^^xsd:int ;
  rdfs:subPropertyOf foaf:account ;
  rdfs:domain :Agent ;
  rdfs:range :EmailAccount .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasGroup
:hasGroup rdf:type owl:ObjectProperty ;
  rdfs:domain :GroupMembership ;
  rdfs:range :Group .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasPerson
:hasPerson rdf:type owl:ObjectProperty ;
  owl:maxCardinality "1"^^xsd:int ;
  rdfs:domain :GroupMembership ;
  rdfs:range :Person .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasGroup
:hasGroup rdf:type owl:ObjectProperty ;
  owl:maxCardinality "1"^^xsd:int ;
  rdfs:domain :GroupMembership ;
  rdfs:range :Group .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasOrg
:hasOrg rdf:type owl:ObjectProperty ;
  rdfs:domain :Affiliation ;
  rdfs:range :Organization .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasContractor
:hasContractor rdf:type owl:ObjectProperty ;
  owl:maxCardinality "1"^^xsd:int ;
  rdfs:domain :Partnership ;
  rdfs:range :Organization .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasContractant
:hasContractant rdf:type owl:ObjectProperty ;
  owl:maxCardinality "1"^^xsd:int ;
  rdfs:domain :Partnership ;
  rdfs:range :Organization .

### foaf:account
foaf:account rdf:type owl:ObjectProperty ;
  owl:maxCardinality "1"^^xsd:int ;
  rdfs:domain :Person ;
  rdfs:range :UserAccount .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasPerson
:hasPerson rdf:type owl:ObjectProperty ;
  rdfs:domain :Affiliation ;
  rdfs:range :Person .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/phone
:phone rdf:type owl:ObjectProperty ;
  owl:maxCardinality "1"^^xsd:int ;
  rdfs:domain :Agent ;
  rdfs:range :Phone .

### sioc:member_of
sioc:member_of rdf:type owl:ObjectProperty ;
  rdfs:domain :UserAccount ;
  rdfs:range :UserGroup .

### sioc:has_member
sioc:has_member rdf:type owl:ObjectProperty ;
  rdfs:domain :UserGroup ;
  rdfs:range :UserAccount .


#################################################################
#    Data properties
#################################################################

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/street1
:street1 rdf:type owl:DatatypeProperty;
  rdfs:domain :Address ;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/street2
:street2 rdf:type owl:DatatypeProperty;
  rdfs:domain :Address ;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/postalCode
:postalCode rdf:type owl:DatatypeProperty;
  rdfs:domain :Address ;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/city
:city rdf:type owl:DatatypeProperty;
  rdfs:domain :Address ;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/country
:country rdf:type owl:DatatypeProperty;
  rdfs:domain :Address ;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/role
:role rdf:type owl:DatatypeProperty;
  rdfs:domain :Affiliation ;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/avatar
:avatar rdf:type owl:DatatypeProperty;
  rdfs:domain :Agent ;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/accountName
:accountName rdf:type owl:DatatypeProperty;
  rdfs:domain :EmailAccount ;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/email
:email rdf:type owl:DatatypeProperty;
  rdfs:domain :EmailAccount ;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/isVerified
:isVerified rdf:type owl:DatatypeProperty;
  rdfs:domain :EmailAccount ;
  rdfs:range <http://www.w3.org/2001/XMLSchema#boolean> .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/isMainEmail
:isMainEmail rdf:type owl:DatatypeProperty;
  rdfs:domain :EmailAccount ;
  rdfs:range <http://www.w3.org/2001/XMLSchema#boolean> .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/name
:name rdf:type owl:DatatypeProperty;
  rdfs:domain :Group ;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/description
:description rdf:type owl:DatatypeProperty;
  rdfs:domain :Group ;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/startDate
:startDate rdf:type owl:DatatypeProperty;
  rdfs:domain :GroupMembership ;
  rdfs:range <http://www.w3.org/2001/XMLSchema#float> .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/endDate
:endDate rdf:type owl:DatatypeProperty;
  rdfs:domain :GroupMembership ;
  rdfs:range <http://www.w3.org/2001/XMLSchema#float> .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/role
:role rdf:type owl:DatatypeProperty;
  rdfs:domain :GroupMembership ;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/name
:name rdf:type owl:DatatypeProperty;
  rdfs:domain :Organization ;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/shortName
:shortName rdf:type owl:DatatypeProperty;
  rdfs:domain :Organization ;
  rdfs:range rdfs:Literal .

### dc:description
dc:description rdf:type owl:DatatypeProperty;
  rdfs:domain :Organization ;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/shortDescription
:shortDescription rdf:type owl:DatatypeProperty;
  rdfs:domain :Organization ;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/other
:other rdf:type owl:DatatypeProperty;
  rdfs:domain :Organization ;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/startDate
:startDate rdf:type owl:DatatypeProperty;
  rdfs:domain :Partnership ;
  rdfs:range <http://www.w3.org/2001/XMLSchema#float> .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/endDate
:endDate rdf:type owl:DatatypeProperty;
  rdfs:domain :Partnership ;
  rdfs:range <http://www.w3.org/2001/XMLSchema#float> .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/role
:role rdf:type owl:DatatypeProperty;
  rdfs:domain :Partnership ;
  rdfs:range rdfs:Literal .

### foaf:firstName
foaf:firstName rdf:type owl:DatatypeProperty;
  rdfs:domain :Person ;
  rdfs:range rdfs:Literal .

### foaf:lastName
foaf:lastName rdf:type owl:DatatypeProperty;
  rdfs:domain :Person ;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/maidenName
:maidenName rdf:type owl:DatatypeProperty;
  rdfs:domain :Person ;
  rdfs:range rdfs:Literal .

### foaf:gender
foaf:gender rdf:type owl:DatatypeProperty;
  rdfs:domain :Person ;
  rdfs:range rdfs:Literal .

### foaf:birthday
foaf:birthday rdf:type owl:DatatypeProperty;
  rdfs:domain :Person ;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/bio
:bio rdf:type owl:DatatypeProperty;
  rdfs:domain :Person ;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/shortBio
:shortBio rdf:type owl:DatatypeProperty;
  rdfs:domain :Person ;
  rdfs:range rdfs:Literal .

### rdfs:label
rdfs:label rdf:type owl:DatatypeProperty;
  rdfs:domain :Phone ;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/number
:number rdf:type owl:DatatypeProperty;
  rdfs:domain :Phone ;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/userId
:userId rdf:type owl:DatatypeProperty;
  rdfs:domain :UserAccount ;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/username
:username rdf:type owl:DatatypeProperty;
  rdfs:domain :UserAccount ;
  rdfs:range rdfs:Literal .



############################################################
#==============   mnx-contribution  ================================#
############################################################


#################################################################
#    Classes
#################################################################

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/Action
:Action rdf:type owl:Class;
  rdfs:subClassOf prov:Activity ; 
  rdfs:label "Action"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/Creation
:Creation rdf:type owl:Class;
  rdfs:subClassOf :Action ;
  rdfs:label "Creation"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/Deletion
:Deletion rdf:type owl:Class;
  rdfs:subClassOf :Action ;
  rdfs:label "Deletion"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/ExternalLink
:ExternalLink rdf:type owl:Class;
  rdfs:label "ExternalLink"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/Tagging
:Tagging rdf:type owl:Class;
  rdfs:subClassOf :OnlineContribution, :Instant, :TemporalEntity, :MaterialEntity, :Entity ;
  rdfs:label "Tagging"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/Update
:Update rdf:type owl:Class;
  rdfs:subClassOf :Action ;
  rdfs:label "Update"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/OnlineContribution
:OnlineContribution rdf:type owl:Class;
  rdfs:subClassOf sioc:Item ; 
  rdfs:subClassOf :Instant, :TemporalEntity, :MaterialEntity, :Entity ;
  rdfs:label "OnlineContribution"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/Comment
:Comment rdf:type owl:Class;
  rdfs:subClassOf sioc:Post ; 
  rdfs:subClassOf :OnlineContribution, :Instant, :TemporalEntity, :MaterialEntity, :Entity ;
  rdfs:label "Comment"@en .


#################################################################
#    Object Properties
#################################################################

### prov:wasAttributedTo
prov:wasAttributedTo rdf:type owl:ObjectProperty ;
  owl:maxCardinality "1"^^xsd:int ;
  rdfs:domain :UserAccount ;
  rdfs:range :Action .

### prov:used
prov:used rdf:type owl:ObjectProperty ;
  owl:maxCardinality "1"^^xsd:int ;
  rdfs:domain :Action ;
  rdfs:range :Entity .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasExternalLink
:hasExternalLink rdf:type owl:ObjectProperty ;
  rdfs:domain :Entity ;
  rdfs:range :ExternalLink .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasConcept
:hasConcept rdf:type owl:ObjectProperty ;
  owl:maxCardinality "1"^^xsd:int ;
  rdfs:domain :Tagging ;
  rdfs:range skos:Concept .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasEntity
:hasEntity rdf:type owl:ObjectProperty ;
  owl:maxCardinality "1"^^xsd:int ;
  rdfs:domain :Tagging ;
  rdfs:range :Entity .

### sioc:hasReply
sioc:hasReply rdf:type owl:ObjectProperty ;
  rdfs:domain :OnlineContribution ;
  rdfs:range :Comment .

### sioc:reply_of
sioc:reply_of rdf:type owl:ObjectProperty ;
  owl:maxCardinality "1"^^xsd:int ;
  rdfs:domain :Comment ;
  rdfs:range :OnlineContribution .


#################################################################
#    Data properties
#################################################################

### prov:startedAtTime
prov:startedAtTime rdf:type owl:DatatypeProperty;
  rdfs:domain :Action ;
  rdfs:range <http://www.w3.org/2001/XMLSchema#dateTimeStamp> .

### prov:endedAtTime
prov:endedAtTime rdf:type owl:DatatypeProperty;
  rdfs:domain :Action ;
  rdfs:range <http://www.w3.org/2001/XMLSchema#dateTimeStamp> .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/link
:link rdf:type owl:DatatypeProperty;
  rdfs:domain :ExternalLink ;
  rdfs:range rdfs:Literal .

### rdfs:label
rdfs:label rdf:type owl:DatatypeProperty;
  rdfs:domain :ExternalLink ;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/comment
:comment rdf:type owl:DatatypeProperty;
  rdfs:domain :Update ;
  rdfs:range rdfs:Literal .

### sioc:content
sioc:content rdf:type owl:DatatypeProperty;
  rdfs:domain :OnlineContribution ;
  rdfs:range rdfs:Literal .

############################################################
#==============   mnx-geo  ================================#
############################################################


#################################################################
#    Classes
#################################################################

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/Geometry
:Geometry rdf:type owl:Class;
  rdfs:subClassOf :Entity ;
  rdfs:label "Geometry"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/Location
:Location rdf:type owl:Class;
  rdfs:subClassOf :SpatialEntity, :MaterialEntity, :Entity ;
  rdfs:label "Location"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/Point
:Point rdf:type owl:Class;
  rdfs:subClassOf :Geometry, :Entity ;
  rdfs:label "Point"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/SpatialEntity
:SpatialEntity rdf:type owl:Class;
  rdfs:subClassOf :MaterialEntity, :Entity ;
  rdfs:label "SpatialEntity"@en .


#################################################################
#    Object Properties
#################################################################

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasGeometry
:hasGeometry rdf:type owl:ObjectProperty ;
  owl:maxCardinality "1"^^xsd:int ;
  rdfs:domain :SpatialEntity ;
  rdfs:range :Geometry .


#################################################################
#    Data properties
#################################################################

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/asWKT
:asWKT rdf:type owl:DatatypeProperty;
  rdfs:domain :Geometry ;
  rdfs:range <http://www.opengis.net/ont/geosparql#wktLiteral> .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/geonamesFeature
:geonamesFeature rdf:type owl:DatatypeProperty;
  rdfs:domain :Location ;
  rdfs:range <http://www.w3.org/2001/XMLSchema#anyURI> .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/alt
:alt rdf:type owl:DatatypeProperty;
  rdfs:domain :Point ;
  rdfs:range <http://www.w3.org/2001/XMLSchema#float> .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/long
:long rdf:type owl:DatatypeProperty;
  rdfs:domain :Point ;
  rdfs:range <http://www.w3.org/2001/XMLSchema#float> .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/lat
:lat rdf:type owl:DatatypeProperty;
  rdfs:domain :Point ;
  rdfs:range <http://www.w3.org/2001/XMLSchema#float> .





############################################################
#==============   mnx-time  ================================#
############################################################


#################################################################
#    Classes
#################################################################

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/Duration
:Duration rdf:type owl:Class;
  rdfs:subClassOf :TemporalEntity, :MaterialEntity, :Entity ;
  rdfs:label "Duration"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/Instant
:Instant rdf:type owl:Class;
  rdfs:subClassOf :TemporalEntity, :MaterialEntity, :Entity ;
  rdfs:label "Instant"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/TemporalEntity
:TemporalEntity rdf:type owl:Class;
  rdfs:subClassOf :MaterialEntity, :Entity ;
  rdfs:label "TemporalEntity"@en .


#################################################################
#    Data properties
#################################################################

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/endDate
:endDate rdf:type owl:DatatypeProperty;
  rdfs:domain :Duration ;
  rdfs:range <http://www.w3.org/2001/XMLSchema#dateTimeStamp> .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/startDate
:startDate rdf:type owl:DatatypeProperty;
  rdfs:domain :Duration ;
  rdfs:range <http://www.w3.org/2001/XMLSchema#dateTimeStamp> .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/occursAt
:occursAt rdf:type owl:DatatypeProperty;
  rdfs:domain :Instant ;
  rdfs:range <http://www.w3.org/2001/XMLSchema#dateTimeStamp> .
